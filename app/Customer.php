<?php

namespace App;
use App\CRUD;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use CRUD;
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'customer_code',
        'customer_name',
        'identity_old',
        'identity',
        'birthday',
        'company',
        'tax_id',
        'address',
        'phone',
        'mobile',
        'debt',
        'note',
    ];

    public function wards()
    {
        return $this->belongsToMany('App\Ward', 'ward_customers', 'customer_id', 'ward_id');
    }

    public function districts()
    {
        return $this->belongsToMany('App\District', 'district_customers', 'customer_id', 'district_id');
    }

    public function cities()
    {
        return $this->belongsToMany('App\City','city_customers', 'customer_id', 'city_id');
    }
}
