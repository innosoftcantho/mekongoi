<?php

namespace App\Http\Controllers\Admin;

use App\District;
use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDistrict;
use App\Http\Requests\UpdateDistrict;

class DistrictController extends Controller
{
    public function __construct(District $model)
    {
        $this->model    = $model;
        $this->slug     = 'districts';
        $this->city     = new City;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.$this->slug.index",[
            'data_table'    => $this->model->read(),
            'route'         => $this->slug,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.$this->slug.create",[
            'route'         => $this->slug,
            'cities'        => $this->city->all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDistrict $request)
    {
        $data = $this->model->create($request->all());
        if ($request->is_async == 1) {
            return [
                'status'    => 'success',
                'message'   => 'Đã lưu!',
                'district'  => $data,
            ];
        }
        return redirect()->route("$this->slug.index")->with('success', 'create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function show(District $district)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function edit(District $district)
    {
        return view("admin.$this->slug.edit",[
            'cities'    => $this->city->all(),
            'data'      => $district,
            'route'     => $this->slug,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDistrict $request, District $district)
    {
        $district->update($request->all());
        return redirect()->route("$this->slug.index")->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function destroy(District $district)
    {
        $district->delete();
        return redirect()->route("$this->slug.index")->with('success', 'delete');
    }
}
