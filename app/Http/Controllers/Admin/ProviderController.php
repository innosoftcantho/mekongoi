<?php

namespace App\Http\Controllers\Admin;
use App\Provider;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProvider;
use App\Http\Requests\UpdateProvider;

class ProviderController extends Controller
{
    public function __construct(Provider $model)
    {
        $this->model    = $model;
        $this->slug     = 'providers';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.$this->slug.index",[
            'data_table'    => $this->model->read(),
            'route'         => $this->slug,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.$this->slug.create",[
            'route'         => $this->slug,
            'users'         => User::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProvider $request)
    {
        $this->model->create($request->all());
        return redirect()->route("$this->slug.index")->with('success', 'create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Provider $provider)
    {
        return view("admin.$this->slug.edit", [
            'data'      => $provider,
            'route'     => $this->slug,
            'users'     => User::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProvider $request, Provider $provider)
    {
        $provider->update($request->all());
        return redirect()->route("$this->slug.index", ['success' => 'update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provider $provider)
    {
        $provider->delete();
        return redirect()->route("$this->slug.index", ['success' => 'delete']);
    }
}
