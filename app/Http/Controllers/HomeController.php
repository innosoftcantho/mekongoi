<?php

namespace App\Http\Controllers;

// use App\Store;
// use Auth;
use App;
use App\Menu;
use App\Type;
use App\MenuType;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->menu = new Menu;
        $this->type = new Type;
        $this->product = new Product;
        $this->menu_type = new MenuType;
        $this->category = new Category;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home', [
        //     'route' => 'home'
        // ]);
        $menu = $this->menu->where('alias', '/')->where('lang', app()->getLocale())->firstOrFail();
        $category = $menu->categories()->firstOrFail();
        $contents = $category->contents()->where('contents.is_show', 1)->orderBy('contents.id', 'desc')->take(2)->get();
        return view("home",
        [
            'categories'    => $this->category->get(),
            // 'category'      => $category,
            'contents'      => $contents,
            'menu'          => $menu,
            // 'cart'          => $this->menu->where('type', 'cart')->where('lang', app()->getLocale())->firstOrFail(),
            'menu_category' => $category->menus()->where('alias', '<>', '/')->first(),
            'menus'         => $this->menu->getMenus(),
            'products'      => $this->product->where('is_show', 1)->orderBy('created_at')->paginate(3),
            'all_products'  => $this->product->get(),
            // 'route'         => $menu->type,
            // 'types'         => $this->type->whereIn('id', $this->menu_type->get()->pluck('type_id'))->orderBy('sort')->get(),
        ]);
    }

    public function menu($alias)
    {
        $menus = explode('/', $alias);
        $countMenu = count($menus);
        if ($countMenu > 2) abort(404);
        $menu = $this->menu->where('alias', $menus[0])->where('lang', app()->getLocale())->firstOrFail();
        if ($countMenu == 2)
            return App::make("App\Http\Controllers\Web\\" . Str::studly($menu->type) . "Controller")->detail($menu, $menus[1]);
        return App::make("App\Http\Controllers\Web\\" . Str::studly($menu->type) . "Controller")->index($menu);
    }
}
