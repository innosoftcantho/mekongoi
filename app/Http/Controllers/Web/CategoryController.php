<?php

namespace App\Http\Controllers\Web;

use App\Content;
use App\Menu;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct(Menu $model, Content $content, Product $product)
    {
        $this->content  = $content;
        $this->menu     = $model;
        $this->product  = $product;
    }

    public function index($menu)
    {
        $category = $menu->categories()->firstOrFail();
        $featured = $category->contents()->where('contents.is_featured', 1)->where('contents.is_show', 1)->get();
        // if ($featured->count() == 0)
            // $featured = $category->contents()->where('contents.is_featured', 0)->where('contents.is_show', 1)->orderBy('contents.id', 'desc')->take(3)->get();
        $contents = $category->contents()->where('contents.is_featured', 0)->where('contents.is_show', 1)->whereNotIn('contents.id', $featured->pluck('id'))->orderBy('contents.id', 'desc')->paginate(5);
        return view("$menu->type",
        [
            'contents'          => $contents,
            'featured_contents' => $featured,
            'category'          => $category,
            'menu'              => $menu,
            'menus'             => $this->menu->getMenus(),
            'route'             => $menu->type,
            'products'          => $this->product->where('is_show', 1)->orderBy('created_at')->paginate(3),
        ]);
    }

    public function detail($menu, $alias)
    {
        $category = $menu->categories()->firstOrFail();
        $content = $this->content->firstAlias($alias);
        $contents = $category->contents()->where('contents.is_show', 1)->where('contents.id', '<>', $content->id)->orderBy('id', 'desc')->paginate(3);
        return view("content",
        [
            'contents'  => $contents,
            'content'   => $content,
            'category'  => $category,
            'menu'      => $menu,
            'all_products'  => $this->product->get(),
            'menus'     => $this->menu->getMenus(),
            'route'     => $menu->type,
        ]);
    }
}