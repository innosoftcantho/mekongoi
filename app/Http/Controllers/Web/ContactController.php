<?php

namespace App\Http\Controllers\Web;

use App\Menu;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->menu = new Menu;
    }

    public function index($menu)
    {
        return view("$menu->type",
        [
            'menu'  => $menu,
            'menus' => $this->menu->getMenus(),
            'all_products'  => $this->product->get(),
            'route' => $menu->type,
        ]);
    }
}