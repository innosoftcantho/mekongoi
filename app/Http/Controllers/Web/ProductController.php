<?php

namespace App\Http\Controllers\Web;
use App\Product;
use App\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function __construct(Menu $model)
    {
        $this->menu = $model;
    }
    
    public function index($menu)
    {
        return view("web.$menu->type",
        [
            'all_products'  => $this->product->get(),
            'product'       => $menu->products()->firstOrFail(),
            'menu'          => $menu,
            'menus'         => $this->menu->getMenus(),
            'route'         => $menu->type,
        ]);
    }
}
