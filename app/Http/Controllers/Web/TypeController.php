<?php

namespace App\Http\Controllers\Web;
use App\Type;
use App\Menu;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypeController extends Controller
{
    public function __construct(Menu $model, Product $product)
    {
        $this->product  = $product;
        $this->menu     = $model;
    }

    public function index($menu)
    {
        $type = $menu->types()->firstOrFail();
        $featured = $type->products()->where('is_featured', 1)->where('is_show', 1)->get();
        // if ($featured->count() == 0)
        //     $featured = $type->products()->where('is_featured', 0)->where('is_show', 1)->orderBy('id', 'desc')->take(3)->get();
        $products = $type->products()->where('is_featured', 0)->where('is_show', 1)->whereNotIn('id', $featured->pluck('id'))->orderBy('id', 'desc')->paginate(5);
        return view("$menu->type",
        [
            'products'          => $products,
            // 'featured_products' => $featured,
            'all_products'      => $this->product->get(),
            'type'              => $type,
            'menu'              => $menu,
            'menus'             => $this->menu->getMenus(),
            'route'             => $menu->type,
        ]);
    }

    public function detail($menu, $alias)
    {
        $type = $menu->types()->firstOrFail();
        // $product = $this->product->firstAlias($alias);
        $product = $this->product->firstOrFail();
        $products = $type->products()->where('is_show', 1)->where('id', '<>', $product->id)->orderBy('id', 'desc')->paginate(3);
        return view("tour",
        [
            'all_products'  => $this->product->get(),
            'products'  => $products,
            'product'   => $product,
            'type'      => $type,
            'menu'      => $menu,
            'menus'     => $this->menu->getMenus(),
            'route'     => $menu->type,
        ]);
    }
}
