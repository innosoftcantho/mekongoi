<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class StoreBill extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'               => 'nullable|integer',
            'customer_id'           => 'nullable|integer',
            'buyer'                 => 'required|string',
            'tax_id'                => 'nullable|string',
            'address'               => 'nullable|string',
            'phone'                 => 'nullable|string',
            'fax'                   => 'nullable|string',
            'symbole'               => 'required|string',
            'bill_number'           => 'required|string',
            'bill_date'             => 'required',
            'customer_name'         => 'nullable|string',
            'customer_company'      => 'nullable|string',
            'customer_tax_id'       => 'nullable|string',
            'customer_address'      => 'nullable|string',
            'payments'              => 'nullable|string',
            'bank'                  => 'nullable|string',
            'discount'              => 'numeric|nullable',
            'discount_total'        => 'numeric|nullable',
            'subtotal'              => 'numeric|nullable',
            'vat'                   => 'numeric|nullable',
            'vat_total'             => 'numeric|nullable',
            'total'                 => 'numeric|nullable',
            'note'                  => 'nullable|',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            // 'payments'              => 'TM/CK',
            'discount'              => 0,
            'discount_total'        => 0,
            'subtotal'              => 0,
            'vat'                   => 0,
            'vat_total'             => 0,
            'total'                 => 0,
            ]);
    }
}
