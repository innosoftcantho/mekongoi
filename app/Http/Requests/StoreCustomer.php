<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class StoreCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'               => 'max:191|nullable',
            'city_id'               => 'required|',
            'company'               => 'nullable:customer_name|max:191|nullable',
            'customer_code'         => 'string|max:191',
            'customer_name'         => 'required|string|max:191',
            'debt'                  => 'numeric|nullable',
            'district_id'           => 'required|',
            'identity'              => 'max:191|nullable',
            'identity_old'          => 'max:191|nullable',
            'mobile'                => 'max:12|nullable',
            'note'                  => 'max:191|nullable',
            'phone'                 => 'max:12|nullable',
            'tax_id'                => 'nullable|',
            
        ];
    }

}
