<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar'        => 'nullable|string|max:191',
            'alias'         => 'required|string|max:191',
            'bill_price'    => '|numeric',
            'description'   => 'nullable|string',
            'detail'        => 'nullable|string|max:191',
            'is_show'       => 'nullable|boolean',
            'note'          => 'nullable|string',
            'origin'        => 'nullable|string|max:30',
            'product_name'  => 'required|string|max:191',
            'receipt_price' => '|numeric',
            'stock'         => '|numeric',
            'unit'          => 'nullable|string',
            'upload'        => 'nullable|file|image|mimes:jpeg,png,gif,bmp|max:2048',
            'video'         => 'nullable|string',
            'warranty'      => 'nullable|string|max:30',
        ];
    }
}
