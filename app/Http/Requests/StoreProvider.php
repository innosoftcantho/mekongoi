<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProvider extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'           => 'nullable|numeric',
            'provider_name'     => 'required|string|max:100',
            'provider_code'     => 'required|string|max:100',
            'tax_id'            => 'nullable|string',
            'address'           => 'nullable|string|max:191',
            'phone'             => 'nullable|string|max:20',
            'mobile'            => 'nullable|string|max:20',
            'debt'              => 'numeric',
            'note'              => 'nullable|string',
        ];
    }
}
