<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreReceipt extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'           => 'nullable|',
            'provider_id'       => 'nullable|',
            'provider_name'     => 'required|',
            'tax_id'            => 'nullable|',
            'address'           => 'nullable|',
            'phone'             => 'nullable|',
            'fax'               => 'nullable|',
            'symbole'           => 'required|',
            'receipt_number'    => 'required|',
            'receipt_date'      => 'required|',
            'subtotal'          => 'nullable|',
            'vat'               => 'nullable|',
            'vat_total'         => 'nullable|',
            'total'             => 'nullable|',
            'note'              => 'nullable|',
        ];
    }
}
