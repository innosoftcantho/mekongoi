<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreWard extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'district_id'   => 'required|string',
            'ward_code'     => 'required|string|max:191',
            'ward_level'    => 'required|string|max:191',
            'ward_name'     => 'required|string|max:191',
            'note'          => 'nullable|string|max:191',
        ];
    }
}
