<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCity extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_code'     => 'nullable|string',
            'city_level'    => 'nullable|string|max:191',
            'city_name'     => 'required|string|max:191',
            'note'          => 'nullable|string|max:191',
        ];
    }
}
