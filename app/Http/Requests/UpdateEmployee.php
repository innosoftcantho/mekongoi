<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'       => 'nullable|numeric',
            'avatar'        => 'nullable|string',
            'upload'        => 'nullable|file|image|mimes:jpeg,png,gif|max:2048',
            'employee_name' => 'required|string|max:191',
            'employee_code' => 'required|string|max:191',
            'identity_old'  => 'nullable|string|max:191',
            'identity'      => 'nullable|string|max:191',
            'birthday'      => 'nullable|string|max:191',
            'address'       => 'nullable|string|max:191',
            'phone'         => 'nullable|string|max:191',
            'mobile'        => 'nullable|string|max:191',
            'note'          => 'nullable|string|max:191',
        ];
    }
}
