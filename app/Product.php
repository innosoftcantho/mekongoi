<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\CRUD;

class Product extends Model
{
    use CRUD;
    use SoftDeletes;

    protected $fillable = [
        'avatar',
        'alias',
        'bill_price',
        'description',
        'detail',
        'embed',
        'is_featured',
        'is_show',
        'note',
        'origin',
        'product_name',
        'show_type',
        'stock',
        'unit',
        'video',
    ];
    public function types()
    {
        return $this->belongsToMany('App\Type', 'App\ProductType');
    }

    public function type()
    {
        return $this->types()->firstOrFail();
    }
}
