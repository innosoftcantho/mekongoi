<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\CRUD;

class Type extends Model
{
    use CRUD;
    use Notifiable;
    use SoftDeletes;
    
    protected $fillable = [
        'avatar',
        'type_name',
        'sort',
        'note',
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product', 'product_types', 'type_id', 'product_id');
    }

    public function menuType()
    {
        return $this->hasOne('App\MenuType')->latest();
    }

    public function menu()
    {
        return self::menuType()->menu;
    }

}
