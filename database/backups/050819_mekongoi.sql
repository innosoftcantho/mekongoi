/*
 Navicat Premium Data Transfer

 Source Server         : connection1
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : mekongoi

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 05/08/2019 14:02:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for advance_payments
-- ----------------------------
DROP TABLE IF EXISTS `advance_payments`;
CREATE TABLE `advance_payments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Người tạo',
  `content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nội dung',
  `total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tổng cộng',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `advance_payments_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `advance_payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for advance_vouchers
-- ----------------------------
DROP TABLE IF EXISTS `advance_vouchers`;
CREATE TABLE `advance_vouchers`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Người tạo',
  `content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nội dung',
  `total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tổng cộng',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `advance_vouchers_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `advance_vouchers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bill_advance_payments
-- ----------------------------
DROP TABLE IF EXISTS `bill_advance_payments`;
CREATE TABLE `bill_advance_payments`  (
  `bill_id` bigint(20) UNSIGNED NOT NULL,
  `advance_payment_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`bill_id`, `advance_payment_id`) USING BTREE,
  INDEX `bill_advance_payments_advance_payment_id_foreign`(`advance_payment_id`) USING BTREE,
  CONSTRAINT `bill_advance_payments_advance_payment_id_foreign` FOREIGN KEY (`advance_payment_id`) REFERENCES `advance_payments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bill_advance_payments_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bill_advance_vouchers
-- ----------------------------
DROP TABLE IF EXISTS `bill_advance_vouchers`;
CREATE TABLE `bill_advance_vouchers`  (
  `bill_id` bigint(20) UNSIGNED NOT NULL,
  `advance_voucher_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`bill_id`, `advance_voucher_id`) USING BTREE,
  INDEX `bill_advance_vouchers_advance_voucher_id_foreign`(`advance_voucher_id`) USING BTREE,
  CONSTRAINT `bill_advance_vouchers_advance_voucher_id_foreign` FOREIGN KEY (`advance_voucher_id`) REFERENCES `advance_vouchers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bill_advance_vouchers_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bill_products
-- ----------------------------
DROP TABLE IF EXISTS `bill_products`;
CREATE TABLE `bill_products`  (
  `bill_id` bigint(20) UNSIGNED NOT NULL,
  `order` bigint(20) UNSIGNED NOT NULL COMMENT 'Số thứ tự',
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Số lượng',
  `price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Đơn giá',
  `subtotal` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Thành tiền',
  `discount` decimal(8, 0) NOT NULL DEFAULT 0 COMMENT 'Chiết khấu',
  `discount_total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tiền chiết khấu',
  `total` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Tổng',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`bill_id`, `order`, `product_id`) USING BTREE,
  INDEX `bill_products_product_id_foreign`(`product_id`) USING BTREE,
  CONSTRAINT `bill_products_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bill_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bill_vouchers
-- ----------------------------
DROP TABLE IF EXISTS `bill_vouchers`;
CREATE TABLE `bill_vouchers`  (
  `bill_id` bigint(20) UNSIGNED NOT NULL,
  `voucher_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`bill_id`, `voucher_id`) USING BTREE,
  INDEX `bill_vouchers_voucher_id_foreign`(`voucher_id`) USING BTREE,
  CONSTRAINT `bill_vouchers_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bill_vouchers_voucher_id_foreign` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bills
-- ----------------------------
DROP TABLE IF EXISTS `bills`;
CREATE TABLE `bills`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Người tạo',
  `customer_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Khách hàng',
  `buyer` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Đơn vị bán hàng',
  `tax_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã số thuế',
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Địa chỉ',
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Điện thoại',
  `fax` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Fax',
  `symbole` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Ký hiệu',
  `bill_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Số',
  `bill_date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT 'Ngày hóa đơn',
  `customer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Họ tên người mua hàng',
  `customer_company` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Đơn vị mua hàng',
  `customer_tax_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã số thuế',
  `customer_address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Địa chỉ',
  `payments` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'TM/CK' COMMENT 'Hình thức thanh toán',
  `bank` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Số tài khoản',
  `discount` decimal(8, 0) NOT NULL DEFAULT 0 COMMENT 'Chiết khấu',
  `discount_total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tiền chiết khấu',
  `subtotal` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Tổng',
  `vat` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT 'VAT',
  `vat_total` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tiền VAT',
  `total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tổng cộng',
  `is_edit` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Cho phép sửa',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `bills_user_id_foreign`(`user_id`) USING BTREE,
  INDEX `bills_customer_id_foreign`(`customer_id`) USING BTREE,
  CONSTRAINT `bills_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bills_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `category_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 1, 0, NULL, 'Gioi thieu', 1, 0, NULL, 'vi', NULL, '2019-08-03 08:23:07', '2019-08-03 08:23:07', NULL);

-- ----------------------------
-- Table structure for cities
-- ----------------------------
DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `city_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã',
  `city_level` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Cấp',
  `city_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên tỉnh/thành phố',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for city_customers
-- ----------------------------
DROP TABLE IF EXISTS `city_customers`;
CREATE TABLE `city_customers`  (
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`city_id`, `customer_id`) USING BTREE,
  INDEX `city_customers_customer_id_foreign`(`customer_id`) USING BTREE,
  CONSTRAINT `city_customers_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `city_customers_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for content_categories
-- ----------------------------
DROP TABLE IF EXISTS `content_categories`;
CREATE TABLE `content_categories`  (
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT 0,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`content_id`, `category_id`) USING BTREE,
  INDEX `content_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `content_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `content_categories_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of content_categories
-- ----------------------------
INSERT INTO `content_categories` VALUES (1, 1, 0, 1, 0, 0, '2019-08-03 09:40:41', '2019-08-03 09:40:41', NULL);
INSERT INTO `content_categories` VALUES (2, 1, 0, 1, 0, 0, '2019-08-03 09:33:45', '2019-08-03 09:33:45', NULL);

-- ----------------------------
-- Table structure for contents
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `is_draft` tinyint(1) NOT NULL DEFAULT 0,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `contents_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `contents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contents
-- ----------------------------
INSERT INTO `contents` VALUES (1, 1, 1, 'upload/contents/1.png', 'Chúng tôi là mekong ơi', 'chung-toi-la-mekong-oi', 'Công ty TNHH Mekong ơi là công ty du lịch địa phương tốt nhất tại thành phố Cần Thơ chuyên về các tour du lịch hướng dẫn nhóm thủ công, nhỏ và thân mật đến Cần Thơ, Châu Đốc ở đồng bằng sông Cửu Long, Việt Nam. Chúng tôi đang cung cấp nhiều tour du lịch nội bộ: Sân bay Tho, vé tàu cao tốc đến Phnom Penh, Phú Quốc, Côn Đảo, dịch vụ vận chuyển tư nhân VIP và hướng dẫn viên du lịch chuyên gia địa phương được cấp phép để phù hợp với tất cả các sở thích và danh sách mong muốn của bạn.', '<p>C&ocirc;ng ty TNHH Mekong ơi l&agrave; c&ocirc;ng ty du lịch địa phương tốt nhất tại th&agrave;nh phố Cần Thơ chuy&ecirc;n về c&aacute;c tour du lịch hướng dẫn nh&oacute;m thủ c&ocirc;ng, nhỏ v&agrave; th&acirc;n mật đến Cần Thơ, Ch&acirc;u Đốc ở đồng bằng s&ocirc;ng Cửu Long, Việt Nam. Ch&uacute;ng t&ocirc;i đang cung cấp nhiều tour du lịch nội bộ: S&acirc;n bay Tho, v&eacute; t&agrave;u cao tốc đến Phnom Penh, Ph&uacute; Quốc, C&ocirc;n Đảo, dịch vụ vận chuyển tư nh&acirc;n VIP v&agrave; hướng dẫn vi&ecirc;n du lịch chuy&ecirc;n gia địa phương được cấp ph&eacute;p để ph&ugrave; hợp với tất cả c&aacute;c sở th&iacute;ch v&agrave; danh s&aacute;ch mong muốn của bạn.</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-08-03 08:25:14', '2019-08-03 09:40:41', NULL);
INSERT INTO `contents` VALUES (2, 2, 1, 'upload/contents/2.jpeg', 'Gioi thiệu thành phố cần thơ', 'gioi-thieu-thanh-pho-can-tho', 'Công ty TNHH Mekong ơi là công ty du lịch địa phương tốt nhất tại thành phố Cần Thơ chuyên về các tour du lịch hướng dẫn nhóm thủ công, nhỏ và thân mật đến Cần Thơ, Châu Đốc ở đồng bằng sông Cửu Long, Việt Nam. Chúng tôi đang cung cấp nhiều tour du lịch nội bộ: Sân bay Tho, vé tàu cao tốc đến Phnom Penh, Phú Quốc, Côn Đảo, dịch vụ vận chuyển tư nhân VIP và hướng dẫn viên du lịch chuyên gia địa phương được cấp phép để phù hợp với tất cả các sở thích và danh sách mong muốn của bạn.', '<p>C&ocirc;ng ty TNHH Mekong ơi l&agrave; c&ocirc;ng ty du lịch địa phương tốt nhất tại th&agrave;nh phố Cần Thơ chuy&ecirc;n về c&aacute;c tour du lịch hướng dẫn nh&oacute;m thủ c&ocirc;ng, nhỏ v&agrave; th&acirc;n mật đến Cần Thơ, Ch&acirc;u Đốc ở đồng bằng s&ocirc;ng Cửu Long, Việt Nam. Ch&uacute;ng t&ocirc;i đang cung cấp nhiều tour du lịch nội bộ: S&acirc;n bay Tho, v&eacute; t&agrave;u cao tốc đến Phnom Penh, Ph&uacute; Quốc, C&ocirc;n Đảo, dịch vụ vận chuyển tư nh&acirc;n VIP v&agrave; hướng dẫn vi&ecirc;n du lịch chuy&ecirc;n gia địa phương được cấp ph&eacute;p để ph&ugrave; hợp với tất cả c&aacute;c sở th&iacute;ch v&agrave; danh s&aacute;ch mong muốn của bạn.</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-08-03 08:26:12', '2019-08-03 09:25:12', NULL);

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Tài khoản',
  `customer_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã khách hàng',
  `customer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Khách hàng',
  `identity_old` varchar(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'CMND',
  `identity` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'CCCD',
  `birthday` timestamp(0) NULL DEFAULT NULL COMMENT 'Ngày sinh',
  `company` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Đơn vị',
  `tax_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã số thuế',
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Địa chỉ',
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Điện thoại',
  `mobile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Di động',
  `debt` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Công nợ',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `customers_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `customers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for district_customers
-- ----------------------------
DROP TABLE IF EXISTS `district_customers`;
CREATE TABLE `district_customers`  (
  `district_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`district_id`, `customer_id`) USING BTREE,
  INDEX `district_customers_customer_id_foreign`(`customer_id`) USING BTREE,
  CONSTRAINT `district_customers_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `district_customers_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for districts
-- ----------------------------
DROP TABLE IF EXISTS `districts`;
CREATE TABLE `districts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `district_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã',
  `district_level` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Cấp',
  `district_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên thành phố/quận/huyện/thị xã',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `districts_city_id_foreign`(`city_id`) USING BTREE,
  CONSTRAINT `districts_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for employee_payments
-- ----------------------------
DROP TABLE IF EXISTS `employee_payments`;
CREATE TABLE `employee_payments`  (
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `payment_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`employee_id`, `payment_id`) USING BTREE,
  INDEX `employee_payments_payment_id_foreign`(`payment_id`) USING BTREE,
  CONSTRAINT `employee_payments_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `employee_payments_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for employees
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Tài khoản',
  `employee_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã nhân viên',
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ảnh đại diện',
  `employee_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Họ tên',
  `identity_old` varchar(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'CMND',
  `identity` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'CCCD',
  `birthday` timestamp(0) NULL DEFAULT NULL COMMENT 'Ngày sinh',
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Địa chỉ',
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Điện thoại',
  `mobile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Di động',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `employees_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `employees_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_categories
-- ----------------------------
DROP TABLE IF EXISTS `menu_categories`;
CREATE TABLE `menu_categories`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `category_id`) USING BTREE,
  INDEX `menu_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `menu_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_categories_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_categories
-- ----------------------------
INSERT INTO `menu_categories` VALUES (1, 1, '2019-08-03 08:30:55', '2019-08-03 08:30:55', NULL);
INSERT INTO `menu_categories` VALUES (5, 1, '2019-08-05 09:14:47', '2019-08-05 09:14:47', NULL);

-- ----------------------------
-- Table structure for menu_contents
-- ----------------------------
DROP TABLE IF EXISTS `menu_contents`;
CREATE TABLE `menu_contents`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `content_id`) USING BTREE,
  INDEX `menu_contents_content_id_foreign`(`content_id`) USING BTREE,
  CONSTRAINT `menu_contents_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_contents_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_contents
-- ----------------------------
INSERT INTO `menu_contents` VALUES (5, 1, '2019-08-03 16:46:31', '2019-08-03 16:46:31', NULL);

-- ----------------------------
-- Table structure for menu_products
-- ----------------------------
DROP TABLE IF EXISTS `menu_products`;
CREATE TABLE `menu_products`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `product_id`) USING BTREE,
  INDEX `menu_products_product_id_foreign`(`product_id`) USING BTREE,
  CONSTRAINT `menu_products_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_types
-- ----------------------------
DROP TABLE IF EXISTS `menu_types`;
CREATE TABLE `menu_types`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `type_id`) USING BTREE,
  INDEX `menu_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `menu_types_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_types
-- ----------------------------
INSERT INTO `menu_types` VALUES (2, 1, '2019-08-02 16:57:17', '2019-08-02 16:57:17', NULL);
INSERT INTO `menu_types` VALUES (3, 2, '2019-08-02 16:57:35', '2019-08-02 16:57:35', NULL);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `menu_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 1, 0, 'Trang chủ', '/', 'home', '', 1, 0, 'vi', NULL, '2019-08-02 16:45:00', '2019-08-02 17:20:08');
INSERT INTO `menus` VALUES (2, 2, 0, 'Tour nhiều ngày', 'tour-nhieu-ngay', 'type', NULL, 1, 0, 'vi', NULL, '2019-08-02 16:57:17', '2019-08-02 16:57:17');
INSERT INTO `menus` VALUES (3, 3, 0, 'Tour một ngày', 'tour-mot-ngay', 'type', NULL, 1, 0, 'vi', NULL, '2019-08-02 16:57:35', '2019-08-02 16:57:35');
INSERT INTO `menus` VALUES (4, 4, 0, 'Liên hệ', 'lien-he', 'contact', NULL, 1, 0, 'vi', NULL, '2019-08-02 17:19:54', '2019-08-02 17:19:54');
INSERT INTO `menus` VALUES (5, 5, 0, 'Bài viết giới thiệu', 'bai-viet-gioi-thieu', 'category', '', 1, 0, 'vi', NULL, '2019-08-02 17:35:40', '2019-08-05 09:14:47');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_06_07_092509_create_employers_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_06_07_092528_create_providers_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_06_07_092536_create_customers_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_06_07_092547_create_types_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_06_07_092557_create_products_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_06_07_092608_create_receipts_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_06_07_092623_create_quotations_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_06_07_092633_create_bills_table', 1);
INSERT INTO `migrations` VALUES (11, '2019_06_07_092656_create_payments_table', 1);
INSERT INTO `migrations` VALUES (12, '2019_06_07_092707_create_vouchers_table', 1);
INSERT INTO `migrations` VALUES (13, '2019_06_07_092743_create_product_types_table', 1);
INSERT INTO `migrations` VALUES (14, '2019_06_07_092802_create_receipt_products_table', 1);
INSERT INTO `migrations` VALUES (15, '2019_06_07_092816_create_quotation_products_table', 1);
INSERT INTO `migrations` VALUES (16, '2019_06_07_092828_create_bill_products_table', 1);
INSERT INTO `migrations` VALUES (17, '2019_06_07_092859_create_bill_vouchers_table', 1);
INSERT INTO `migrations` VALUES (18, '2019_06_07_171748_create_advance_payments_table', 1);
INSERT INTO `migrations` VALUES (19, '2019_06_07_172006_create_advance_vouchers_table', 1);
INSERT INTO `migrations` VALUES (20, '2019_06_07_172233_create_provider_payments_table', 1);
INSERT INTO `migrations` VALUES (21, '2019_06_07_174504_create_employer_payments_table', 1);
INSERT INTO `migrations` VALUES (22, '2019_06_11_150012_create_employees_table', 1);
INSERT INTO `migrations` VALUES (23, '2019_06_14_105904_delete_employer_payments_table', 1);
INSERT INTO `migrations` VALUES (24, '2019_06_14_110313_delete_employers_table', 1);
INSERT INTO `migrations` VALUES (25, '2019_06_14_110347_create_employee_payments_table', 1);
INSERT INTO `migrations` VALUES (26, '2019_06_14_112733_create_receipt_bill_products_table', 1);
INSERT INTO `migrations` VALUES (27, '2019_06_14_113141_create_bill_advance_payments_table', 1);
INSERT INTO `migrations` VALUES (28, '2019_06_14_113153_create_bill_advance_vouchers_table', 1);
INSERT INTO `migrations` VALUES (29, '2019_06_14_113500_fix_bills_table', 1);
INSERT INTO `migrations` VALUES (30, '2019_06_14_120304_add_stock_receipt_products_table', 1);
INSERT INTO `migrations` VALUES (31, '2019_06_21_004736_add_provider_code_providers_table', 1);
INSERT INTO `migrations` VALUES (32, '2019_06_21_004901_add_customer_code_customers_table', 1);
INSERT INTO `migrations` VALUES (33, '2019_06_21_004931_add_employee_code_employees_table', 1);
INSERT INTO `migrations` VALUES (34, '2019_06_21_004956_add_product_code_products_table', 1);
INSERT INTO `migrations` VALUES (35, '2019_06_21_100556_add_order_is_edit_receipt_products_table', 1);
INSERT INTO `migrations` VALUES (36, '2019_06_21_100712_add_order_is_edit_bill_products_table', 1);
INSERT INTO `migrations` VALUES (37, '2019_06_21_111547_add_order_receipt_bill_products_table', 1);
INSERT INTO `migrations` VALUES (38, '2019_06_25_094040_remove_is_edit_bill_products_table', 1);
INSERT INTO `migrations` VALUES (39, '2019_06_25_094226_remove_is_edit_receipt_products_table', 1);
INSERT INTO `migrations` VALUES (40, '2019_06_25_094336_add_is_edit_receipts_table', 1);
INSERT INTO `migrations` VALUES (41, '2019_06_25_094339_add_is_edit_bills_table', 1);
INSERT INTO `migrations` VALUES (42, '2019_07_11_101707_create_cities_table', 1);
INSERT INTO `migrations` VALUES (43, '2019_07_11_101826_create_districts_table', 1);
INSERT INTO `migrations` VALUES (44, '2019_07_11_102315_create_wards_table', 1);
INSERT INTO `migrations` VALUES (45, '2019_07_11_102423_create_city_customers_table', 1);
INSERT INTO `migrations` VALUES (46, '2019_07_11_102442_create_district_customers_table', 1);
INSERT INTO `migrations` VALUES (47, '2019_07_11_102448_create_ward_customers_table', 1);
INSERT INTO `migrations` VALUES (48, '2019_07_11_154011_add_detail_unit_to_products_table', 1);
INSERT INTO `migrations` VALUES (49, '2019_07_12_005150_allow_null_symbole_receipts_table', 2);
INSERT INTO `migrations` VALUES (50, '2019_07_12_142538_add_warranty_to_products_table', 2);
INSERT INTO `migrations` VALUES (51, '2019_07_13_134438_add_origin_to_products_table', 2);
INSERT INTO `migrations` VALUES (52, '2019_07_15_171057_create_languages_table', 2);
INSERT INTO `migrations` VALUES (53, '2019_07_15_172233_create_menus_table', 2);
INSERT INTO `migrations` VALUES (54, '2019_07_15_173740_create_contents_table', 2);
INSERT INTO `migrations` VALUES (55, '2019_07_16_101957_create_categories_table', 2);
INSERT INTO `migrations` VALUES (56, '2019_07_16_102230_create_menu_categories_table', 2);
INSERT INTO `migrations` VALUES (57, '2019_07_16_102328_create_menu_contents_table', 2);
INSERT INTO `migrations` VALUES (58, '2019_07_16_102429_create_content_categories_table', 2);
INSERT INTO `migrations` VALUES (59, '2019_07_16_151845_allow_null_target_menus_table', 2);
INSERT INTO `migrations` VALUES (60, '2019_07_19_161724_create_menu_products_table', 2);
INSERT INTO `migrations` VALUES (61, '2019_07_19_161841_create_menu_types_table', 2);
INSERT INTO `migrations` VALUES (62, '2019_07_22_084928_change_null_cities_table', 2);
INSERT INTO `migrations` VALUES (63, '2019_07_22_085020_change_null_districts_table', 2);
INSERT INTO `migrations` VALUES (64, '2019_07_22_085047_change_null_wards_table', 2);
INSERT INTO `migrations` VALUES (67, '2019_08_03_102812_add_is_featured_show_type_to_products_table', 3);
INSERT INTO `migrations` VALUES (74, '2019_08_03_105123_add_video_to_products_table', 4);
INSERT INTO `migrations` VALUES (75, '2019_08_05_094005_add_alias_to_products_table', 5);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Người tạo',
  `content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nội dung',
  `total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tổng cộng',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `payments_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for product_types
-- ----------------------------
DROP TABLE IF EXISTS `product_types`;
CREATE TABLE `product_types`  (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`, `type_id`) USING BTREE,
  INDEX `product_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `product_types_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `product_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product_types
-- ----------------------------
INSERT INTO `product_types` VALUES (2, 2, NULL, NULL);
INSERT INTO `product_types` VALUES (3, 1, NULL, NULL);
INSERT INTO `product_types` VALUES (3, 2, NULL, NULL);
INSERT INTO `product_types` VALUES (4, 1, NULL, NULL);
INSERT INTO `product_types` VALUES (5, 1, NULL, NULL);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ảnh đại diện',
  `video` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Video',
  `product_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã sản phẩm',
  `product_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại sản phẩm',
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Alias',
  `detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Chi tiết',
  `unit` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Đơn vị tính',
  `receipt_price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Giá nhập',
  `bill_price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Giá bán',
  `stock` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Tồn kho',
  `origin` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Xuất xứ',
  `warranty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Bảo hành',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Mô tả',
  `show_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Cách hiển thị',
  `is_featured` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Đặc biệt',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Hiển thị',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `embed` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Embed',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 'upload/products/1.jpeg', NULL, 'a', 'aa', '', NULL, NULL, 0, 1200000, 0, NULL, NULL, '<p>Lorem</p>', 0, 0, 1, NULL, '2019-08-03 09:31:25', '2019-08-03 09:31:34', '2019-08-03 09:31:34', NULL);
INSERT INTO `products` VALUES (2, 'upload/products/2.jpeg', NULL, '1', 'CAN THO FLOATING MARKET & WILDLIFE1', 'can-tho-floating-market-wildlife1', NULL, NULL, 0, 1200000, 0, NULL, NULL, '<ul>\r\n	<li>Lorem ipsum dolor sit amet, consetetur</li>\r\n	<li>Lorem ipsum dolor sit amet, consetetur</li>\r\n	<li>Lorem ipsum dolor sit amet, consetetur</li>\r\n	<li>Lorem ipsum dolor sit amet, consetetur</li>\r\n	<li>Lorem ipsum dolor sit amet, consetetur</li>\r\n</ul>', 0, 0, 1, NULL, '2019-08-03 09:45:38', '2019-08-05 10:05:55', NULL, NULL);
INSERT INTO `products` VALUES (3, 'upload/products/3.jpeg', NULL, '2', 'CAN THO FLOATING MARKET & WILDLIFE', 'can-tho-floating-market-wildlife', NULL, NULL, 0, 1200000, 0, NULL, NULL, '<ul>\r\n	<li>Lorem ipsum dolor sit amet, consetetur</li>\r\n	<li>Lorem ipsum dolor sit amet, consetetur</li>\r\n	<li>Lorem ipsum dolor sit amet, consetetur</li>\r\n	<li>Lorem ipsum dolor sit amet, consetetur</li>\r\n	<li>Lorem ipsum dolor sit amet, consetetur</li>\r\n</ul>', 0, 0, 1, NULL, '2019-08-03 09:46:23', '2019-08-05 10:05:36', NULL, NULL);
INSERT INTO `products` VALUES (4, 'upload/products/4.jpeg', 'https://www.youtube.com/watch?v=xXDzgx2QTqA', NULL, 'Du lịch bụi', 'du-lich-bui', NULL, NULL, 0, 4, 0, NULL, NULL, '<p>Lorem</p>', 1, 0, 1, NULL, '2019-08-03 10:48:21', '2019-08-05 10:43:40', NULL, 'xXDzgx2QTqA');
INSERT INTO `products` VALUES (5, 'upload/products/5.jpeg', NULL, NULL, 'Câu cá trên sông Hậu', 'cau-ca-tren-song-hau', NULL, NULL, 0, 1200000, 0, 'Việt Nam', NULL, NULL, 0, 0, 1, NULL, '2019-08-03 14:40:18', '2019-08-05 09:53:23', NULL, NULL);

-- ----------------------------
-- Table structure for provider_payments
-- ----------------------------
DROP TABLE IF EXISTS `provider_payments`;
CREATE TABLE `provider_payments`  (
  `provider_id` bigint(20) UNSIGNED NOT NULL,
  `payment_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`provider_id`, `payment_id`) USING BTREE,
  INDEX `provider_payments_payment_id_foreign`(`payment_id`) USING BTREE,
  CONSTRAINT `provider_payments_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `provider_payments_provider_id_foreign` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for providers
-- ----------------------------
DROP TABLE IF EXISTS `providers`;
CREATE TABLE `providers`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Tài khoản',
  `provider_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã nhà cung cấp',
  `provider_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nhà cung cấp',
  `tax_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã số thuế',
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Địa chỉ',
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Điện thoại',
  `mobile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Di động',
  `debt` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Công nợ',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `providers_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `providers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for quotation_products
-- ----------------------------
DROP TABLE IF EXISTS `quotation_products`;
CREATE TABLE `quotation_products`  (
  `quotation_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Số lượng',
  `price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Đơn giá',
  `subtotal` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Thành tiền',
  `discount` decimal(8, 0) NOT NULL DEFAULT 0 COMMENT 'Chiết khấu',
  `discount_total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tiền chiết khấu',
  `total` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Tổng',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`quotation_id`, `product_id`) USING BTREE,
  INDEX `quotation_products_product_id_foreign`(`product_id`) USING BTREE,
  CONSTRAINT `quotation_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `quotation_products_quotation_id_foreign` FOREIGN KEY (`quotation_id`) REFERENCES `quotations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for quotations
-- ----------------------------
DROP TABLE IF EXISTS `quotations`;
CREATE TABLE `quotations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Người tạo',
  `customer_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Khách hàng',
  `order_date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT 'Ngày đặt',
  `customer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Họ tên người mua hàng',
  `customer_company` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Đơn vị mua hàng',
  `tax_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã số thuế',
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Địa chỉ',
  `payments` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'TM/CK' COMMENT 'Hình thức thanh toán',
  `bank` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Số tài khoản',
  `discount` decimal(8, 0) NOT NULL DEFAULT 0 COMMENT 'Chiết khấu',
  `discount_total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tiền chiết khấu',
  `subtotal` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Tổng',
  `vat` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT 'VAT',
  `vat_total` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tiền VAT',
  `total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tổng cộng',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `quotations_user_id_foreign`(`user_id`) USING BTREE,
  INDEX `quotations_customer_id_foreign`(`customer_id`) USING BTREE,
  CONSTRAINT `quotations_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `quotations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for receipt_bill_products
-- ----------------------------
DROP TABLE IF EXISTS `receipt_bill_products`;
CREATE TABLE `receipt_bill_products`  (
  `receipt` bigint(20) UNSIGNED NOT NULL COMMENT 'ID phiếu nhập',
  `order` bigint(20) UNSIGNED NOT NULL COMMENT 'STT phiếu nhập',
  `bill` bigint(20) UNSIGNED NOT NULL COMMENT 'ID phiếu xuất',
  `product` bigint(20) UNSIGNED NOT NULL COMMENT 'ID sản phẩm',
  `quantity` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Số lượng',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`receipt`, `order`, `bill`, `product`) USING BTREE,
  INDEX `receipt_bill_products_bill_foreign`(`bill`) USING BTREE,
  INDEX `receipt_bill_products_product_foreign`(`product`) USING BTREE,
  CONSTRAINT `receipt_bill_products_bill_foreign` FOREIGN KEY (`bill`) REFERENCES `bills` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `receipt_bill_products_product_foreign` FOREIGN KEY (`product`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `receipt_bill_products_receipt_foreign` FOREIGN KEY (`receipt`) REFERENCES `receipts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for receipt_products
-- ----------------------------
DROP TABLE IF EXISTS `receipt_products`;
CREATE TABLE `receipt_products`  (
  `receipt_id` bigint(20) UNSIGNED NOT NULL,
  `order` bigint(20) UNSIGNED NOT NULL COMMENT 'Số thứ tự',
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Số lượng',
  `price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Đơn giá',
  `subtotal` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Thành tiền',
  `discount` decimal(8, 0) NOT NULL DEFAULT 0 COMMENT 'Chiết khấu',
  `discount_total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tiền chiết khấu',
  `total` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Tổng',
  `stock` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Tồn kho',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`receipt_id`, `order`, `product_id`) USING BTREE,
  INDEX `receipt_products_product_id_foreign`(`product_id`) USING BTREE,
  CONSTRAINT `receipt_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `receipt_products_receipt_id_foreign` FOREIGN KEY (`receipt_id`) REFERENCES `receipts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for receipts
-- ----------------------------
DROP TABLE IF EXISTS `receipts`;
CREATE TABLE `receipts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Người tạo',
  `provider_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Nhà cung cấp',
  `provider_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Đơn vị',
  `tax_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã số thuế',
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Địa chỉ',
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Điện thoại',
  `fax` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Fax',
  `symbole` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Ký hiệu',
  `receipt_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Số',
  `receipt_date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT 'Ngày hóa đơn',
  `subtotal` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Tổng',
  `vat` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT 'VAT',
  `vat_total` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tiền VAT',
  `total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tổng cộng',
  `is_edit` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Cho phép sửa',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `receipts_user_id_foreign`(`user_id`) USING BTREE,
  INDEX `receipts_provider_id_foreign`(`provider_id`) USING BTREE,
  CONSTRAINT `receipts_provider_id_foreign` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `receipts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for types
-- ----------------------------
DROP TABLE IF EXISTS `types`;
CREATE TABLE `types`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ảnh đại diện',
  `type_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại sản phẩm',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Hiển thị',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT 'Sắp xếp',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of types
-- ----------------------------
INSERT INTO `types` VALUES (1, NULL, 'Tour nhiều ngày', 1, 0, NULL, '2019-08-02 16:43:10', '2019-08-02 16:43:10', NULL);
INSERT INTO `types` VALUES (2, NULL, 'Tour một ngày', 1, 0, NULL, '2019-08-02 16:43:21', '2019-08-02 16:43:21', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'admin', 'innosofttestmail@gmail.com', NULL, '$2y$10$fd84OOFPOv4yCfpJZEt1juJDCT0NT5vJTtnYqHFx8IaBx/OutUAle', 1, 'OVrI31FyADpnKJLCXZBMgqOycBWA2PD3cpUxTsO1HrpDlcdcSUUbfPqm2MxK', '2019-04-16 10:36:20', '2019-08-05 08:49:34', NULL);

-- ----------------------------
-- Table structure for vouchers
-- ----------------------------
DROP TABLE IF EXISTS `vouchers`;
CREATE TABLE `vouchers`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Người tạo',
  `content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nội dung',
  `total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tổng cộng',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `vouchers_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `vouchers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ward_customers
-- ----------------------------
DROP TABLE IF EXISTS `ward_customers`;
CREATE TABLE `ward_customers`  (
  `ward_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ward_id`, `customer_id`) USING BTREE,
  INDEX `ward_customers_customer_id_foreign`(`customer_id`) USING BTREE,
  CONSTRAINT `ward_customers_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ward_customers_ward_id_foreign` FOREIGN KEY (`ward_id`) REFERENCES `wards` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wards
-- ----------------------------
DROP TABLE IF EXISTS `wards`;
CREATE TABLE `wards`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `district_id` bigint(20) UNSIGNED NOT NULL,
  `ward_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã',
  `ward_level` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Cấp',
  `ward_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên phường/xã/thị trấn',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `wards_district_id_foreign`(`district_id`) USING BTREE,
  CONSTRAINT `wards_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
