/*
 Navicat Premium Data Transfer

 Source Server         : connection1
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : mekongoi

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 06/08/2019 16:36:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `category_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NULL DEFAULT 0,
  `sort` tinyint(4) NOT NULL DEFAULT 0,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 1, 0, '', 'Gioi thieu', 1, 0, NULL, 'vi', NULL, '2019-08-06 10:09:17', '2019-08-06 10:09:17', NULL);
INSERT INTO `categories` VALUES (2, 2, 0, '', 'Gioi thieu thanh pho', 1, 0, NULL, 'vi', NULL, '2019-08-06 14:35:02', '2019-08-06 14:35:02', NULL);

-- ----------------------------
-- Table structure for content_categories
-- ----------------------------
DROP TABLE IF EXISTS `content_categories`;
CREATE TABLE `content_categories`  (
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT 0,
  `is_show` tinyint(1) NOT NULL DEFAULT 0,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` bigint(20) NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of content_categories
-- ----------------------------
INSERT INTO `content_categories` VALUES (2, 1, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `content_categories` VALUES (1, 1, 0, 0, 0, 0, NULL, NULL);

-- ----------------------------
-- Table structure for contents
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `is_show` tinyint(1) NULL DEFAULT 0,
  `is_draft` tinyint(1) NULL DEFAULT 0,
  `is_featured` tinyint(1) NULL DEFAULT 0,
  `sort` tinyint(4) NOT NULL,
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'vi',
  `views` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contents
-- ----------------------------
INSERT INTO `contents` VALUES (1, 1, 1, 'upload/contents/1.jpeg', 'Gioi theiu mekongei', 'gioi-theiu-mekongei', 'Công ty TNHH Mekong ơi là công ty du lịch địa phương tốt nhất tại thành phố Cần Thơ chuyên về các tour du lịch hướng dẫn nhóm thủ công, nhỏ và thân mật đến Cần Thơ, Châu Đốc ở đồng bằng sông', '<p>GIỚI THIỆU VỀ C&Ocirc;NG TY TNHH DU LỊCH S&Ocirc;NG</p>\r\n\r\n<p>C&ocirc;ng ty TNHH du lịch s&ocirc;ng Cần Thơ l&agrave; c&ocirc;ng ty du lịch địa phương tốt nhất tại th&agrave;nh phố Cần Thơ chuy&ecirc;n về c&aacute;c tour du lịch hướng dẫn nh&oacute;m thủ c&ocirc;ng, nhỏ v&agrave; th&acirc;n mật đến Cần Thơ, Ch&acirc;u Đốc ở đồng bằng s&ocirc;ng Cửu Long, Việt Nam. Ch&uacute;ng t&ocirc;i đang cung cấp nhiều tour v&agrave; hoạt động nội bộ: Tour đi xe đạp, tour thuyền cổ điển, tour xe m&aacute;y phi&ecirc;u lưu (tr&ecirc;n lưng xe tay ga), tour kh&aacute;m ph&aacute;, tour ẩm thực đường phố Cần Thơ, tour du lịch hoang d&atilde; &amp; rừng, ở nh&agrave; địa phương, đ&oacute;n kh&aacute;ch S&acirc;n bay Tho, v&eacute; t&agrave;u cao tốc đến Phnom Penh, Ph&uacute; Quốc, C&ocirc;n Đảo, dịch vụ vận chuyển tư nh&acirc;n VIP v&agrave; hướng dẫn vi&ecirc;n du lịch chuy&ecirc;n gia địa phương được cấp ph&eacute;p để ph&ugrave; hợp với tất cả c&aacute;c sở th&iacute;ch du lịch v&agrave; danh s&aacute;ch mong muốn của bạn.</p>\r\n\r\n<p>CH&Uacute;NG TA L&Agrave; AI</p>\r\n\r\n<p>Xin ch&agrave;o! T&ocirc;i l&agrave; Thomas. C&acirc;u chuyện của t&ocirc;i bắt đầu v&agrave;o những ng&agrave;y đầu ti&ecirc;n khi t&ocirc;i trở th&agrave;nh sinh vi&ecirc;n du lịch tại Đại học Cần Thơ năm 2006. V&agrave;o thời điểm đ&oacute;, t&ocirc;i hiểu s&acirc;u sắc c&aacute;c gi&aacute; trị của du lịch kh&aacute;m ph&aacute; v&agrave; đặc biệt l&agrave; du lịch dựa v&agrave;o cộng đồng với du lịch c&oacute; tr&aacute;ch nhiệm. V&igrave; vậy, t&ocirc;i rất muốn phổ biến những điều phi thường của qu&ecirc; hương cho bạn th&ocirc;ng qua c&aacute;c tour du lịch th&uacute; vị, độc đ&aacute;o v&agrave; c&oacute; kinh nghiệm. &Yacute; ch&iacute; của t&ocirc;i đ&atilde; được nu&ocirc;i dưỡng v&agrave; m&agrave;i giũa qua bốn năm đại học v&agrave; bảy năm kinh nghiệm l&agrave;m việc với kh&aacute;ch du lịch quốc tế trong một m&ocirc;i trường tuyệt vời v&agrave; chuy&ecirc;n nghiệp.</p>\r\n\r\n<p>T&ocirc;i đ&atilde; th&agrave;nh lập Cantho River Tour v&agrave;o đầu th&aacute;ng 7 năm 2017. C&aacute;c tour du lịch của ch&uacute;ng t&ocirc;i cung cấp cho du kh&aacute;ch đi xa hơn v&agrave; xa hơn để đến chợ nổi kh&ocirc;ng biến thể, v&ugrave;ng n&ocirc;ng th&ocirc;n với những ng&ocirc;i l&agrave;ng của những vườn c&acirc;y phong ph&uacute;, c&aacute;nh đồng l&uacute;a v&ocirc; bi&ecirc;n, thực phẩm địa phương, động vật hoang d&atilde; v&agrave; rừng.</p>\r\n\r\n<p>T&ocirc;i lu&ocirc;n tự h&agrave;o về Cantho River Tour, một trong những cơ quan du lịch đầu ti&ecirc;n nổi tiếng với phẩm gi&aacute;, chất lượng v&agrave; tr&aacute;ch nhiệm đối với sự ph&aacute;t triển bền vững của cộng đồng v&agrave; kh&aacute;ch du lịch.</p>\r\n\r\n<p>Tour du lịch s&ocirc;ng Cantho mong được ch&agrave;o đ&oacute;n kh&aacute;ch du lịch v&agrave; sẵn s&agrave;ng phục vụ tất cả c&aacute;c bạn trong suốt thời gian lưu tr&uacute; tại Cần Thơ để mang đến cho bạn niềm vui, niềm vui v&agrave; lu&ocirc;n l&agrave;m h&agrave;i l&ograve;ng du kh&aacute;ch 100% - trước chuyến tham quan, trong tour, sau chuyến tham quan v&agrave; tour tiếp theo.</p>\r\n\r\n<p>Cảm ơn bạn</p>\r\n\r\n<p>Đội du lịch s&ocirc;ng Cantho</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-06 10:09:34', '2019-08-06 14:46:46', NULL);
INSERT INTO `contents` VALUES (2, 2, 1, 'upload/contents/2.jpeg', 'gioi thieu dbscl', 'gioi-thieu-dbscl', 'Công ty TNHH Mekong ơi là công ty du lịch địa phương tốt nhất tại thành phố Cần Thơ chuyên về các tour du lịch hướng dẫn nhóm thủ công, nhỏ và thân mật đến Cần Thơ, Châu Đốc ở đồng bằng sông', '<ul>\r\n	<li>C&ocirc;ng ty TNHH Mekong ơi l&agrave; c&ocirc;ng ty du lịch địa phương tốt nhất tại th&agrave;nh phố</li>\r\n	<li>Cần Thơ chuy&ecirc;n về c&aacute;c tour du lịch hướng dẫn nh&oacute;m thủ c&ocirc;ng, nhỏ v&agrave; th&acirc;n mật</li>\r\n	<li>đến Cần Thơ, Ch&acirc;u Đốc ở đồng bằng s&ocirc;ng&nbsp;</li>\r\n</ul>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-06 10:21:12', '2019-08-06 14:37:22', NULL);

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lang` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NULL DEFAULT 0,
  `note` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_categories
-- ----------------------------
DROP TABLE IF EXISTS `menu_categories`;
CREATE TABLE `menu_categories`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `category_id`) USING BTREE,
  INDEX `menu_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `menu_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_categories_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_categories
-- ----------------------------
INSERT INTO `menu_categories` VALUES (1, 1, '2019-08-06 10:09:55', '2019-08-06 10:09:55', NULL);
INSERT INTO `menu_categories` VALUES (5, 2, '2019-08-06 14:36:30', '2019-08-06 14:36:30', NULL);
INSERT INTO `menu_categories` VALUES (6, 1, '2019-08-06 16:06:39', '2019-08-06 16:06:39', NULL);

-- ----------------------------
-- Table structure for menu_contents
-- ----------------------------
DROP TABLE IF EXISTS `menu_contents`;
CREATE TABLE `menu_contents`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `content_id`) USING BTREE,
  INDEX `menu_contents_content_id_foreign`(`content_id`) USING BTREE,
  CONSTRAINT `menu_contents_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_contents_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_contents
-- ----------------------------
INSERT INTO `menu_contents` VALUES (2, 1, '2019-08-06 14:44:36', '2019-08-06 14:44:36', NULL);

-- ----------------------------
-- Table structure for menu_products
-- ----------------------------
DROP TABLE IF EXISTS `menu_products`;
CREATE TABLE `menu_products`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `product_id`) USING BTREE,
  INDEX `menu_products_product_id_foreign`(`product_id`) USING BTREE,
  CONSTRAINT `menu_products_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_types
-- ----------------------------
DROP TABLE IF EXISTS `menu_types`;
CREATE TABLE `menu_types`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `type_id`) USING BTREE,
  INDEX `menu_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `menu_types_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_types
-- ----------------------------
INSERT INTO `menu_types` VALUES (3, 1, '2019-08-06 10:37:37', '2019-08-06 10:37:37', NULL);
INSERT INTO `menu_types` VALUES (4, 2, '2019-08-06 10:38:03', '2019-08-06 10:38:03', NULL);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `menu_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '\"\"',
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` tinyint(4) NOT NULL,
  `lang` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 1, 0, 'Trang chu', '/', 'home', NULL, 1, 0, 'vi', NULL, '2019-08-06 10:09:55', '2019-08-06 10:09:55');
INSERT INTO `menus` VALUES (2, 2, 0, 'Gioi thieu', 'gioi-thieu', 'about', '', 1, 0, 'vi', NULL, '2019-08-06 10:15:51', '2019-08-06 14:44:06');
INSERT INTO `menus` VALUES (3, 3, 0, 'Du lich nhieu ngay', 'du-lich-nhieu-ngay', 'type', NULL, 1, 0, 'vi', NULL, '2019-08-06 10:37:37', '2019-08-06 10:37:37');
INSERT INTO `menus` VALUES (4, 4, 0, 'du lich trong ngay', 'du-lich-trong-ngay', 'type', NULL, 1, 0, 'vi', NULL, '2019-08-06 10:38:03', '2019-08-06 10:38:03');
INSERT INTO `menus` VALUES (5, 5, 0, 'Gioi thieu thanh pho', 'gioi-thieu-thanh-pho', 'category', '', 0, 0, 'vi', NULL, '2019-08-06 14:36:22', '2019-08-06 14:36:30');
INSERT INTO `menus` VALUES (6, 6, 0, 'danh muc gioi thieu', 'danh-muc-gioi-thieu', 'category', NULL, 1, 0, 'vi', NULL, '2019-08-06 16:06:38', '2019-08-06 16:06:38');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (15, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (16, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (17, '2019_08_05_144914_create_types_table', 1);
INSERT INTO `migrations` VALUES (18, '2019_08_05_145358_create_products_table', 1);
INSERT INTO `migrations` VALUES (19, '2019_08_05_145452_create_categories_table', 1);
INSERT INTO `migrations` VALUES (20, '2019_08_05_145513_create_contents_table', 1);
INSERT INTO `migrations` VALUES (21, '2019_08_05_145727_create_languages_table', 1);
INSERT INTO `migrations` VALUES (22, '2019_08_05_145742_create_menus_table', 1);
INSERT INTO `migrations` VALUES (23, '2019_08_05_145809_create_menu_types_table', 1);
INSERT INTO `migrations` VALUES (24, '2019_08_05_145846_create_menu_products_table', 1);
INSERT INTO `migrations` VALUES (25, '2019_08_05_145908_create_menu_categories_table', 1);
INSERT INTO `migrations` VALUES (26, '2019_08_05_145917_create_menu_contents_table', 1);
INSERT INTO `migrations` VALUES (27, '2019_08_05_150021_create_product_types_table', 1);
INSERT INTO `migrations` VALUES (28, '2019_08_06_092818_create_content_categories_table', 1);
INSERT INTO `migrations` VALUES (30, '2019_08_06_152530_create_price_ranges_table', 2);
INSERT INTO `migrations` VALUES (31, '2019_08_06_153317_create_product_price_ranges_table', 2);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for price_ranges
-- ----------------------------
DROP TABLE IF EXISTS `price_ranges`;
CREATE TABLE `price_ranges`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `price_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `price` decimal(10, 0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for product_price_ranges
-- ----------------------------
DROP TABLE IF EXISTS `product_price_ranges`;
CREATE TABLE `product_price_ranges`  (
  `price_range_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`, `price_range_id`) USING BTREE,
  INDEX `product_price_ranges_price_range_id_foreign`(`price_range_id`) USING BTREE,
  CONSTRAINT `product_price_ranges_price_range_id_foreign` FOREIGN KEY (`price_range_id`) REFERENCES `price_ranges` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `product_price_ranges_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for product_types
-- ----------------------------
DROP TABLE IF EXISTS `product_types`;
CREATE TABLE `product_types`  (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`, `type_id`) USING BTREE,
  INDEX `product_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `product_types_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `product_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product_types
-- ----------------------------
INSERT INTO `product_types` VALUES (2, 1, NULL, NULL, NULL);
INSERT INTO `product_types` VALUES (2, 2, NULL, NULL, NULL);
INSERT INTO `product_types` VALUES (3, 1, NULL, NULL, NULL);
INSERT INTO `product_types` VALUES (4, 1, NULL, NULL, NULL);
INSERT INTO `product_types` VALUES (4, 2, NULL, NULL, NULL);
INSERT INTO `product_types` VALUES (5, 2, NULL, NULL, NULL);
INSERT INTO `product_types` VALUES (6, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bill_price` double NULL DEFAULT 0,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `detail` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `embed` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `is_featured` tinyint(1) NULL DEFAULT 0,
  `is_show` tinyint(1) NULL DEFAULT 0,
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `origin` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `product_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `show_type` tinyint(4) NOT NULL DEFAULT 0,
  `stock` int(10) UNSIGNED NULL DEFAULT 0,
  `unit` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `video` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, '', 'du-lich-song-nuoc', 0, NULL, '', NULL, 0, 1, NULL, NULL, 'Du lich song nuoc', 0, 0, NULL, NULL, '2019-08-06 10:32:15', '2019-08-06 10:33:05', '2019-08-06 10:33:05');
INSERT INTO `products` VALUES (2, 'upload/products/2.jpeg', 'du-lich-song-nuoc', 0, '<ul>\r\n	<li>&nbsp;define the inverse of a many-to-many relationship,</li>\r\n	<li>you place another call to&nbsp;<code>belongsToMany</code>on your related model.</li>\r\n	<li>To continue our user roles example, let&#39;s define the&nbsp;<code>users</code>&nbsp;method</li>\r\n	<li>on the&nbsp;<code>Role</code>&nbsp;model:</li>\r\n</ul>', NULL, NULL, 0, 1, NULL, NULL, 'Du lich song nuoc', 0, 0, NULL, NULL, '2019-08-06 10:32:58', '2019-08-06 14:38:52', NULL);
INSERT INTO `products` VALUES (3, 'upload/products/3.jpeg', 'du-lich-tren-bo', 0, '<ul>\r\n	<li>&nbsp;define the inverse of a many-to-many relationship,</li>\r\n	<li>you place another call to&nbsp;<code>belongsToMany</code>on your related model.</li>\r\n	<li>To continue our user roles example, let&#39;s define the&nbsp;<code>users</code>&nbsp;method</li>\r\n</ul>', NULL, NULL, 0, 1, NULL, NULL, 'Du lich tren bo', 0, 0, NULL, NULL, '2019-08-06 10:33:29', '2019-08-06 14:38:42', NULL);
INSERT INTO `products` VALUES (4, 'upload/products/4.jpeg', 'du-lich-song-nuoc', 120000, '<ul>\r\n	<li>&nbsp;define the inverse of a many-to-many relationship,</li>\r\n	<li>you place another call to&nbsp;<code>belongsToMany</code>on your related model.</li>\r\n	<li>To continue our user roles example, let&#39;s define the&nbsp;<code>users</code>&nbsp;method</li>\r\n	<li>on the&nbsp;<code>Role</code>&nbsp;model:</li>\r\n</ul>', NULL, '3LOX2-VuIZc', 0, 1, NULL, NULL, 'Du lich song nuoc', 1, 0, NULL, 'https://www.youtube.com/watch?v=3LOX2-VuIZc', '2019-08-06 11:02:38', '2019-08-06 14:38:05', NULL);
INSERT INTO `products` VALUES (5, 'upload/products/5.jpeg', 'du-lich-song-nuoc-2', 30000000, '<ul>\r\n	<li>&nbsp;define the inverse of a many-to-many relationship,</li>\r\n	<li>you place another call to&nbsp;<code>belongsToMany</code>on your related model.</li>\r\n	<li>To continue our user roles example, let&#39;s define the&nbsp;<code>users</code>&nbsp;method</li>\r\n	<li>on the&nbsp;<code>Role</code>&nbsp;model:</li>\r\n</ul>', NULL, NULL, 0, 1, NULL, NULL, 'Du lich song nuoc 2', 0, 0, NULL, NULL, '2019-08-06 11:03:51', '2019-08-06 14:38:32', NULL);
INSERT INTO `products` VALUES (6, 'upload/products/6.jpeg', 'du-lich-thanh-pho', 20000000, '<ul>\r\n	<li>&nbsp;define the inverse of a many-to-many relationship,</li>\r\n	<li>you place another call to&nbsp;<code>belongsToMany</code>on your related model.</li>\r\n	<li>To continue our user roles example, let&#39;s define the&nbsp;<code>users</code>&nbsp;method</li>\r\n	<li>on the&nbsp;<code>Role</code>&nbsp;model:</li>\r\n</ul>', NULL, 'NhJrf3F1hCI', 0, 1, NULL, NULL, 'Du lich thanh pho', 1, 0, NULL, 'https://www.youtube.com/watch?v=NhJrf3F1hCI', '2019-08-06 11:04:38', '2019-08-06 14:38:24', NULL);

-- ----------------------------
-- Table structure for types
-- ----------------------------
DROP TABLE IF EXISTS `types`;
CREATE TABLE `types`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên loại',
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT 'Avatar',
  `sort` tinyint(4) NOT NULL COMMENT 'Sắp xếp',
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'text' COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of types
-- ----------------------------
INSERT INTO `types` VALUES (1, 'Du lich nhieu ngay', '', 0, NULL, '2019-08-06 10:24:27', '2019-08-06 10:24:27', NULL);
INSERT INTO `types` VALUES (2, 'Du lich trong ngay', '', 0, NULL, '2019-08-06 10:24:36', '2019-08-06 10:24:36', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'admin', 'innosofttestmail@gmail.com', NULL, '$2y$10$d6CzWTCCkuD9Mx3bdOdWG.ZOgO2drLYJTCfm/aiK0BpWF53pHJK0K', 1, 'lxG4tRJ8vj9XUaCZHF1lMtLeSSpiWK22tfMtAm5WMNO65NmD9Ld5MYvX04gL', NULL, '2019-08-06 14:02:47', NULL);

SET FOREIGN_KEY_CHECKS = 1;
