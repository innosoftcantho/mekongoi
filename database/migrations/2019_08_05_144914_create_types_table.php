<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('type_name', 191)->comment('Tên loại');
            $table->string('avatar', 191)->nullable()->default('')->comment('Avatar');
            $table->tinyInteger('sort')->comment('Sắp xếp');
            $table->string('note', 191)->nullable()->default('text')->comment('Ghi chú');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types');
    }
}
