<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('avatar', 191)->nullable()->default('');
            $table->string('alias', 191)->nullable(false);
            $table->decimal('bill_price', 10, 0)->nullable()->default(0);
            $table->longText('description')->nullable();
            $table->string('detail', 191)->nullable()->default('');
            $table->string('embed', 191)->nullable()->default('');
            $table->boolean('is_featured')->nullable()->default(0);
            $table->boolean('is_show')->nullable()->default(0);
            $table->string('note', 191)->nullable()->default('');
            $table->string('origin', 191)->nullable()->default('');
            $table->string('product_name', 191)->nullable()->default('');
            $table->tinyInteger('show_type')->default(0);
            $table->integer('stock')->unsigned()->nullable()->default(0);
            $table->string('unit', 20)->nullable()->default('');
            $table->string('video', 191)->nullable()->default('');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
