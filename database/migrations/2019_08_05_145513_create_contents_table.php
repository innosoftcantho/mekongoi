<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('content_id');
            $table->unsignedBigInteger('user_id');
            $table->string('avatar', 191)->nullable()->default('');
            $table->string('title', 191);
            $table->string('alias', 191);
            $table->string('summary', 191)->nullable()->default('');
            $table->longText('content')->nullable();
            $table->boolean('is_show')->nullable()->default(false);
            $table->boolean('is_draft')->nullable()->default(false);
            $table->boolean('is_featured')->nullable()->default(false);
            $table->tinyInteger('sort');
            $table->string('tags', 191)->nullable()->default('');
            $table->text('description')->nullable();
            $table->string('lang', 20)->nullable()->default('vi');
            $table->unsignedBigInteger('views')->default(0);
            $table->string('note', 191)->nullable()->default('');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
