<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPriceRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_price_ranges', function (Blueprint $table) {
            $table->unsignedBigInteger('price_range_id');
            $table->unsignedBigInteger('product_id');

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('price_range_id')->references('id')->on('price_ranges')->onDelete('cascade');

            $table->timestamps();
            $table->primary(['product_id', 'price_range_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('product_price_ranges_price_range_id_foreign');
        $table->dropForeign('product_price_ranges_product_id_foreign');
        Schema::dropIfExists('product_price_ranges');
    }
}
