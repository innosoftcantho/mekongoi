@extends('layouts.web')
@section('content')

<div class="about">
    <div class="bg-center bg-cover" style="background-image: url( {{ asset($about->avatar) }} )">
        <div class="bg-overlay text-center header">
            <h1 class="centered text-uppercase text-white font-weight-bold">{{ $about->title }}</h1>
        </div>
    </div>
    <div class="container my-5">
        <div class="font16px text-justify">
            {!! $about->content !!}
        </div>
    </div>
</div>

@endsection