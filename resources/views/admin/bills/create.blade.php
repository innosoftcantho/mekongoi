@extends('layouts.admin')

@section('title', 'INNOSOFT')

@section('content')
<form method="POST" action="{{ route('bills.store') }}" role="form">
@method('POST')
@csrf
    <!-- sub-Navigation -->
    <nav class="navbar navbar-light bg-white shadow">
        <div class="navbar-brand">{{ __('admin/bills.detail') }}</div>
        <div class="mr-auto">
            <a class="btn btn-outline-dark my-0" href="{{ url()->previous() == url()->current() ? route($route . '.index') : url()->previous() }}">{{ __('admin/table.back') }}</a>
        </div>
        <button type="submit" class="btn btn-success">{{ __('admin/table.save') }}</button>
    </nav>
    <!-- sub-Navigation -->
    <!-- Content -->
    <div class="bg-white my-3 p-3 shadow">
        <h3>{{ __('admin\bills.bill_info') }}</h3>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="symbole">{{ __('admin/bills.symbole') }} (*)</label>
                    <input required type="text" class="form-control" name="symbole" id="symbole" placeholder="{{ __('admin/bills.symbole') }}" maxlength="191" value="{{ old('symbole', $latest_bill->symbole ?? "") }}">
                </div>
                <div class="form-group">
                    <input type="number" hidden name="user_id" value="{{ Auth::user()->id }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="bill_number">{{ __('admin/bills.bill_number') }} (*)</label>
                    <input required type="number" class="form-control" name="bill_number" id="bill_number" placeholder="{{ __('admin/bills.bill_number') }}" maxlength="191" value="{{ old('bill_number', isset($latest_bill) ? $latest_bill->bill_number +1 : 1) }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="bill_date_pick">{{ __('admin/bills.bill_date') }}</label>
                    <input type="text" class="form-control" name="bill_date_pick" id="bill_date_pick" placeholder="{{ __('admin/bills.bill_date') }}" maxlength="191" value="{{ old('bill_date') }}">
                    <input type="text" name="bill_date" id="bill_date" value="{{ now() }}" hidden>
                </div>
            </div>
        </div>
    </div>
    
    <div class="bg-white my-3 p-3 shadow">
        <h3>{{ __('admin\bills.company_info') }}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="buyer">{{ __('admin/bills.buyer') }} (*)</label>
                    <input required type="text" class="form-control" name="buyer" id="buyer" placeholder="{{ __('admin/bills.buyer') }}" maxlength="191" value="{{ old('buyer') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tax_id">{{ __('admin/bills.tax_id')  }}</label>
                    <input type="text" class="form-control" name="tax_id" id="tax_id" placeholder="{{ __('admin/bills.tax_id') }}" maxlength="191" value="{{ old('tax_id') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="address">{{ __('admin/bills.address') }}</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="{{ __('admin/bills.address') }}" maxlength="191" value="{{ old('address') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="phone">{{ __('admin/bills.phone') }}</label>
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="{{ __('admin/bills.phone') }}" maxlength="191" value="{{ old('phone') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="fax">{{ __('admin/bills.fax') }}</label>
                    <input type="text" class="form-control" name="fax" id="fax" placeholder="{{ __('admin/bills.fax') }}" maxlength="191" value="{{ old('fax') }}">
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white my-3 p-3 shadow">
        <h3>{{ __('admin\bills.payment_info') }}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="payment">{{ __('admin/bills.payment') }}</label>
                    <input type="text" class="form-control" name="payment" id="payment" placeholder="{{ __('admin/bills.payment') }}" maxlength="191" value="{{ old('payment', 'TM/CK') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="bank">{{ __('admin/bills.bank') }}</label>
                    <input type="text" class="form-control" name="bank" id="bank" placeholder="{{ __('admin/bills.bank') }}" maxlength="191" value="{{ old('bank') }}">
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white my-3 p-3 shadow">
        <h3>{{ __('admin\bills.customer_info') }}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="customer_id">{{ __('admin/bills.customer_id') }} (*)</label>
                    <select name="customer_id" class="form-control" id="customer_id">
                        <option value="" disabled selected> Chọn khách hàng</option>
                        @foreach ($customers as $customer)
                            <option value="{{ $customer->id }}" data-customer-tax-id="{{ $customer->tax_id }}" data-customer-company="{{ $customer->company }}">{{ $customer->customer_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="customer_name">{{ __('admin/bills.customer_name') }}</label>
                    <input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="{{ __('admin/bills.customer_name') }}" maxlength="191" value="{{ old('customer_name') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="customer_company">{{ __('admin/bills.customer_company') }}</label>
                    <input type="text" class="form-control" name="customer_company" id="customer_company" placeholder="{{ __('admin/bills.customer_company') }}" maxlength="191" value="{{ old('customer_company') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="customer_tax_id">{{ __('admin/bills.customer_tax_id') }}</label>
                    <input type="text" class="form-control" name="customer_tax_id" id="customer_tax_id" placeholder="{{ __('admin/bills.customer_tax_id') }}" maxlength="191" value="{{ old('customer_tax_id') }}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="note">{{ __('admin/bills.note') }}</label>
                    <textarea class="form-control" name="note" id="note" rows="3" placeholder="{{ __('admin/bills.note') }}" value="{{ old('note')}}" ></textarea>
                </div>
            </div>
        </div>
    </div>
    <!-- Content -->
</form>
@endsection

@push('js')
    <script>
        $(function() {
            $('#bill_date_pick').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'),10),
                locale: {
                    format: 'DD/MM/YYYY'
                }
            }, function(start, end, label) {
                $("#bill_date").val(start.format("YYYY-MM-DD"))
            })
        });
        $("#customer_id").on("change", function(){
            var selected_customer = $(this).children("option:selected");
            $("#customer_name").val(selected_customer.text());
            $("#customer_tax_id").val(selected_customer.attr("data-customer-tax-id"));
            $("#customer_company").val(selected_customer.attr("data-customer-company"));
        });
    </script>
@endpush

@push('ready')
    @if (count($errors) > 0)
    toastr["error"]("@foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach");
    @endif
    $("#customer_id").val("{{ old('customer_id') }}")
@endpush