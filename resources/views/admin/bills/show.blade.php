@extends('layouts.admin')

@section('title', 'INNOSOFT')

@section('content')
<!-- sub-Navigation -->
<nav class="navbar navbar-light bg-white shadow">
    <div class="navbar-brand">{{ 'Phiếu xuất số ' . $data->bill_number. ' ngày ' . date('d/m/Y', strtotime($data->bill_date)) }}</div>
    <div class="mr-auto">
        <a class="btn btn-outline-dark my-0" href="{{ url()->previous() == url()->current() ? route($route . '.index') : url()->previous() }}">{{ __('admin/table.back') }}</a>
    </div>
    <button type="submit" class="btn btn-success">{{ __('admin/table.save') }}</button>
</nav>
<!-- sub-Navigation -->
<!-- Content -->
<!--bill product content -->
<div class="bg-white div-content my-3 p-3 shadow">
    <div class="row" id="bill">
        <div class="col-md-12 table-responsive mb-3">
            <table class="table table-hover mb-0" id="bill_products">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col" class="text-center">#</th>
                        <th scope="col">{{ __('admin\products.product_name') }}</th>
                        <th scope="col"class="text-center" style="width: 10%;">{{ __('admin\bill-products.quantity') }}</th>
                        <th scope="col"class="text-right" style="width: 15%;">{{ __('admin\bill-products.price') }}</th>
                        <th scope="col"class="text-right" style="width: 15%;">{{ __('admin\bill-products.discount') }}</th>
                        <th scope="col"class="text-right" style="width: 15%;">{{ __('admin\bill-products.discount_total') }}</th>
                        <th scope="col"class="text-right" style="width: 15%;">{{ __('admin\bill-products.subtotal') }}</th>
                        <th scope="col" style="width: 3rem;"></th>
                        <th scope="col" style="width: 3rem;"></th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($bill_products) == 0)
                    <tr id="no_data">
                        <td colspan="15"><center>{{ __('admin/table.no_data') }}</center></td>
                    </tr>
                    @endif
                    @foreach ($bill_products as $index => $bill_product)
                    <tr class="edit" data-product-id="{{ $bill_product->product_id }}" id="order_{{ $bill_product->order }}">
                        <th scope="row" class="text-center">{{ $index + 1 }}</th>
                        <td>
                            <span class="bg-transparent border-0 form-control">
                                {{ $bill_product->product->product_name}}
                            </span>
                        </td>
                        <td>
                            <span class="bg-transparent border-0 form-control text-center format-quantity">
                                {{ number_format($bill_product->quantity) }}
                            </span>
                            <input type="number" class="form-control text-center quantity" min="1" value="{{ $bill_product->quantity }}">
                        </td>
                        <td>
                            <span class="bg-transparent border-0 form-control text-right format-price">
                                {{ number_format($bill_product->price) }}
                            </span>
                            <input type="number" class="form-control text-right price" min="0" value="{{ $bill_product->price }}">
                        </td>
                        <td>
                            <span class="bg-transparent border-0 form-control text-right format-discount">
                                {{ number_format($bill_product->discount) }}
                            </span>
                            <input type="number" class="form-control text-right discount" value="{{ $bill_product->discount }}">
                        </td>
                        <td>
                            <span class="bg-transparent border-0 form-control text-right format-discount-total">
                                {{ number_format($bill_product->discount_total) }}
                            </span>
                            <input type="number" class="form-control text-right discount-total" value="{{ $bill_product->discount_total }}">
                        </td>
                        <td>
                            <span class="bg-transparent border-0 form-control text-right format-subtotal">
                                {{ number_format($bill_product->subtotal) }}
                            </span>
                            <input hidden type="number" class="form-control text-right subtotal" value="{{ $bill_product->subtotal }}">
                        </td>
                        <td class="px-1">
                            <div class="btn btn-primary">
                                <i class="fas fa-pencil-alt"></i>
                                <i class="fas fa-save"></i>
                            </div>
                        </td>
                        <td class="px-1">
                            <div class="btn btn-danger" data-save="0" data-toggle="modal" data-target="#cfmDel" onclick="$('#delete_order').val($(this).closest('tr').index() + 1)">
                                <i class="fa fa-trash"></i>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="btn btn-success" data-toggle="modal" data-target="#products_modal" id="new_row">
                <i class="fas fa-plus"></i> Thêm sản phẩm
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 offset-md-4">
            <div class="form-group">
                <label for="subtotal">{{ __('admin/bills.subtotal') }}</label>
                <input type="text" class="form-control" name="subtotal" id="subtotal" placeholder="{{ __('admin/bills.subtotal') }}" maxlength="191" value="{{ old('subtotal', 0) }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="subtotal_with_discount_total">Tổng với chiết khấu</label>
                <input type="text" class="form-control" id="subtotal_with_discount_total" placeholder="Tổng với chiết khấu" maxlength="191" value="1" min="0">
            </div>
        </div>
        <div class="col-md-4 offset-md-4">
            <div class="form-group">
                <label for="vat">{{ __('admin/bills.vat') }}</label>
                <input type="text" class="form-control" name="vat" id="vat" placeholder="{{ __('admin/bills.vat') }}" maxlength="191" value="{{ old('vat', 0) }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="vat_total">{{ __('admin/bills.vat_total') }}</label>
                <input type="text" class="form-control" name="vat_total" id="vat_total" placeholder="{{ __('admin/bills.vat_total') }}" maxlength="191" value="{{ old('vat_total') }}">
            </div>
        </div>
        <div class="col-md-4 offset-md-4">
            <div class="form-group">
                <label for="discount">{{ __('admin/bills.discount') }}</label>
                <input type="text" class="form-control" name="discount" id="discount" placeholder="{{ __('admin/bills.discount') }}" maxlength="191" value="{{ old('discount', 0) }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="discount_total">{{ __('admin/bills.discount_total') }}</label>
                <input type="text" class="form-control" name="discount_total" id="discount_total" placeholder="{{ __('admin/bills.discount_total') }}" maxlength="191" min="0" value="{{ old('discount_total', 0) }}">
            </div>
        </div>
        <div class="col-md-8 offset-md-4">
            <div class="form-group">
                <label for="total">{{ __('admin/bills.total') }}</label>
                <input type="text" class="form-control" name="total" id="total" placeholder="{{ __('admin/bills.total') }}" maxlength="191" value="{{ old('total') }}">
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="products_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Sản phẩm</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body search-group">
                <div class="input-group mb-3 search-bar">
                        <input type="text" name="product_code" class="form-control col-md-4" placeholder="Mã sản phẩm">
                        <input type="text" name="product_name" class="form-control" placeholder="Tên sản phẩm">
                    <div class="input-group-append">
                        <div class="btn btn-outline-dark" id="product_search"><i class="fas fa-search"></i></div>
                    </div>
                </div>
            </div>
            <div class="modal-body" id="products_table">
                
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        
        </div>
    </div>
</div>
<div class="modal" id="cfmDel" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="cfmDel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h3>{{ __('admin/table.question_delete') }}</h3><br>
                <input type="text" value="0" id="delete_order" />
                <button type="button" class="btn btn-danger" id="delete" data-dismiss="modal">{{ __('admin/table.delete') }}</button>
                <button class="btn btn-default" data-dismiss="modal">{{ __('admin/table.cancel') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- Content -->
@endsection

@push('js')
<script>
    var stt = {{ count($bill_products) }};
    var filters = [];

    // FUNCTION
    function formatNumber(val) {
        return ("" + parseInt(val != "" ? val : 0)).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    }

    function update_subtotal() {
        var sum = 0;
        $("#bill_products .subtotal").each(function () {
            sum += parseInt($(this).val());
        });
        $("#subtotal").val(sum).trigger("change");
        $("#subtotal_with_discount_total").val(sum).trigger("change");
    }

    function update_total () {
        $("#total").val( Number($("#vat_total").val()) + Number($("#subtotal_with_discount_total").val()) );
    }

    function update_products (url = "{{ route('products.read') }}", page = 1) {
        axios.post(url, {
            page: page,
            filters: filters,
        })
            .then(function (response) {
                var data = response.data;
                $("#products_table").html(data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    
    function delete_bill_product(btn) {
        $("#delete_order").val(btn.closest("tr").index() + 1);
        if(btn.closest("tr").hasClass("store")){
            btn.closest("tr").remove();
        }
    }

    //ROW EVENT
    $("#products_table").on("click", "tr", function () {
        $("#no_data").hide();
        $("#products_modal").modal("hide");
        $(this).addClass("table-danger");
        var product_id = $(this).attr('data-id');
        var price = $(this).attr('data-price');
        stt++;
        $("#bill_products").append(
            '<tr class="store" data-product-id="' + product_id + '" id="order_' + stt + '">' +
                '<th scope="row" class="text-center">' + stt + '</th>' +
                '<td><span class="bg-transparent border-0 form-control">' + $(this).attr('data-product-name') + '</span></td>' +
                '<td><span class="bg-transparent border-0 form-control text-center format-quantity">1</span><input type="number" class="form-control text-center quantity" min="1" max="' + $(this).attr('data-stock') + '" value="1"></td>' +
                '<td><span class="bg-transparent border-0 form-control text-right format-price">' + formatNumber(price) + '</span><input type="number" class="form-control text-right price" min="0" value="' + price + '"></td>' +
                '<td><span class="bg-transparent border-0 form-control text-right format-discount"></span><input type="number" class="form-control text-right discount" min="0" value="0"></td>' +
                '<td><span class="bg-transparent border-0 form-control text-right format-discount-total"></span><input type="number" class="form-control text-right discount-total" min="0" value="0"></td>' +
                '<td><span class="bg-transparent border-0 form-control text-right format-subtotal">' + formatNumber(price) + '</span><input hidden type="number" class="form-control text-right subtotal" value="' + price + '"></td>' +
                '<td class="px-1"><div class="btn btn-success"><i class="fas fa-pencil-alt"></i><i class="fas fa-save"></div></td>' +
                '<td class="px-1"><div class="btn btn-danger" data-toggle="modal" data-target="#cfmDel" onclick="delete_bill_product($(this))"> <i class="fa fa-trash"></i> </div></td>' +
            "</tr>"
        );
        update_subtotal();
    });

    $("#products_modal").on("show.bs.modal", function(e) {
        $('.store .btn-success').click();
    });

    $("#bill_products").on("click", ".store .btn-success", function() {
        var button = $(this);
        var dataRow = $(this).closest("tr");
        axios.post("{{ route('bill-products.store') }}", {
            product_id: dataRow.attr("data-product-id"),
            bill_id: {{ $data->id }},
            order: dataRow.find("th").text(),
            quantity: dataRow.find(".quantity").val(),
            price: dataRow.find(".price").val(),
        })
        .then(function (response) {
            button.removeClass("btn-success").addClass("btn-primary").closest("tr").removeClass("store").addClass("edit");
            toastr[response.data.status](response.data.message);
        })
        .catch(function (error) {
            toastr["error"](error.response.data.message);
        });
    });
    
    $("#bill_products").on("click", ".edit .btn-primary", function() {
        $(this).removeClass("btn-primary").addClass("btn-info").closest("tr").removeClass("edit").addClass("update");
    });

    $("#bill_products").on("click", ".update .btn-info", function() {
        var button = $(this);
        var dataRow = $(this).closest("tr");
        axios.put("{{ route('bill-products.update', ['id' => $data->id]) }}", {
            product_id: dataRow.attr("data-product-id"),
            bill_id: {{ $data->id }},
            quantity:  dataRow.find(".quantity").val(),
            price: dataRow.find(".price").val(),
        })
        .then(function (response) {
            button.removeClass("btn-info").addClass("btn-primary").closest("tr").removeClass("update").addClass("edit");
            toastr[response.data.status](response.data.message);
        })
        .catch(function (error) {
            console.log(error);
        });
    });

    $("#delete").click(function() {
        var order = $("#delete_order").val();
        var dataRow = $("#order_" + order);
        axios.post("{{ route('bill-products.destroy') }}", {
            _method: "DELETE",
            bill_id: {{ $data->id }},
            order: order,
            product_id: dataRow.attr("data-product-id"),
        })
        .then(function (response) {
            dataRow.remove();
            stt--;
            for (var i = order; i <= $("#bill_products tbody tr").length; i++){
                $("#order_" + (parseInt(i) + 1) + " th").html(i);
                $("#order_" + (parseInt(i) + 1)).attr("id", "order_" + i);
            }
            toastr[response.data.status](response.data.message);
        })
        .catch(function (error) {
            toastr["error"](error.response.data.message);
        });
    });
    // STAT ROW EVENT
    $("#bill_products tbody").on("change keyup keydown",".quantity, .price, .discount, .discount-total", function () {
        var dataRow = $(this).closest("tr");
        var quantity = dataRow.find(".quantity").val();
        var price = dataRow.find(".price").val();
        var discount = dataRow.find(".discount").val();
        var discount_total = dataRow.find(".discount-total").val();
        var subtotal = quantity * price;

        if($(this).hasClass("discount-total"))
            discount = discount_total * 100 / subtotal;
        else
            discount_total = (subtotal * discount) /100;

        dataRow.find(".format-quantity").html(formatNumber(quantity));
        dataRow.find(".format-price").html(formatNumber(price));
        dataRow.find(".discount").val(discount);
        dataRow.find(".format-discount").html(formatNumber(discount));
        dataRow.find(".discount-total").val(discount_total);
        dataRow.find(".format-discount-total").html(formatNumber(discount_total));
        dataRow.find(".subtotal").val(subtotal - discount_total);
        dataRow.find(".format-subtotal").html(formatNumber(subtotal - discount_total));
        update_subtotal();
    });

    //STAT EVENT
    $("#subtotal").on("change keyup keydown", function () {
        $("#subtotal_with_discount_total").val($(this).val()).trigger("change");
        $("#discount").change();
    });

    $("#subtotal_with_discount_total").on("change", function () {
        $("#vat").change();
        update_total();
    });

    // VAT GROUP CHANGE
    $("#vat").on("change keyup keydown", function () {
        $("#vat_total").val( Number($("#vat").val()) * Number($("#subtotal_with_discount_total").val()) /100 );
        $("#vat_total").change();
    });

    $("#vat_total").on("keyup keydown change", function () {
        if($("#subtotal_with_discount_total").val() != 0)
        $("#vat").val($(this).val() * 100 / $("#subtotal_with_discount_total").val());
        else
        $("#vat").val(0);
        update_total();
    });
    // DISCOUNT GROUP CHANGE
    $("#discount").on("change keyup keydown", function () {
        var subtotal = $("#subtotal").val();
        $("#discount_total").val( Number($("#discount").val()) * Number(subtotal) /100 ).trigger("change");
    });

    $("#discount_total").on("change keyup keydown", function () {
        var temp_subtotal = $("#subtotal").val();
        if(temp_subtotal != 0)
        $("#discount").val($(this).val() *100 / temp_subtotal);
        else
        $("#discount").val(0);
        $("#subtotal_with_discount_total").val( temp_subtotal - Number($(this).val()) ).trigger("change");
    });
    
    $(".search-group").on("click", "#product_search", function (){
        filters.length = 0;
        $(".search-bar input").each(function () {
            if($(this).val() != "")
            filters.push({
                name: $(this).attr("name"),
                operator: "LIKE",
                value: '%'+$(this).val()+'%',
            })
        })
        update_products (url = "{{ route('products.read') }}", page = 1);
    });

    $("#products_table").on("click", ".page-link", function(e){
        e.preventDefault();
    });
    $("#subtotal_with_discount_total").on("keyup change keydown", function(e){
        e.preventDefault();
    });

</script>
@endpush

@push('ready')
update_products();
@if (session('success'))
toastr["success"]("{{ __('message.' . session('success')) }}");
@endif
@if (session('error'))
toastr["error"]("{{ __('message.' . session('error')) }}");
@endif
update_subtotal();
@endpush