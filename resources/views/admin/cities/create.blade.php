@extends('layouts.admin')

@section('title', 'INNOSOFT')

@section('content')
<form method="POST" action="{{ route($route . '.store') }}" role="form" enctype="multipart/form-data">
@method('POST')
@csrf
    <nav class="navbar navbar-light bg-white shadow">
        <div class="navbar-brand">{{ __('admin/cities.detail') }}</div>
        <div class="mr-auto">
            <a class="btn btn-outline-dark my-0" href="{{ url()->previous() == url()->current() ? route($route . '.index') : url()->previous() }}">{{ __('admin/table.back') }}</a>
        </div>
        <button type="submit" class="btn btn-success">{{ __('admin/table.save') }}</button>
    </nav>
    <div class="bg-white div-content my-3 p-3 shadow">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="city_code">{{ __('admin/cities.city_code') }}</label>
                    <input type="text" class="form-control" name="city_code" id="city_code" placeholder="{{ __('admin/cities.city_code') }}" maxlength="191" value="{{ old('city_code') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="city_level">{{ __('admin/cities.city_level') }}</label>
                    <input type="text" class="form-control" name="city_level" id="city_level" placeholder="{{ __('admin/cities.city_level') }}" maxlength="191" value="{{ old('city_level') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="city_name">{{ __('admin/cities.city_name') }} (*)</label>
                    <input type="text" class="form-control" name="city_name" id="city_name" required placeholder="{{ __('admin/cities.city_name') }}" maxlength="191" value="{{ old('city_name') }}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="note">{{ __('admin/cities.note') }}</label>
                    <textarea class="form-control" name="note" id="note" placeholder="{{ __('admin/cities.note') }}" maxlength="191" rows="5">{{ old('note') }}</textarea>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@push('js')
    <script>
    </script>
@endpush

@push('ready')
    @if (count($errors) > 0)
    toastr["error"]("@foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach");
    @endif
    bsCustomFileInput.init();
@endpush