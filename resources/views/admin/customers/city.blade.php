<!-- cityModal -->
<div class="modal fade" id="cityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cityModalLabel">Tạo mới thành phố</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="city_code">{{ __('admin/cities.city_code') }} (*)</label>
                    <input type="text" class="form-control" name="city_code" id="city_code" placeholder="{{ __('admin/cities.city_code') }}" maxlength="191" value="">
                </div>
                <div class="form-group">
                    <label for="city_level">{{ __('admin/cities.city_level') }} (*)</label>
                    <input type="text" class="form-control" name="city_level" id="city_level" placeholder="{{ __('admin/cities.city_level') }}" maxlength="191" value="">
                </div>
                <div class="form-group">
                    <label for="city_name">{{ __('admin/cities.city_name') }} (*)</label>
                    <input type="text" class="form-control" name="city_name" id="city_name" required placeholder="{{ __('admin/cities.city_name') }}" maxlength="191" value="">
                </div>
                <div class="form-group">
                    <label for="note">{{ __('admin/cities.note') }}</label>
                    <textarea class="form-control" name="note" id="note" rows="3" placeholder="{{ __('admin/cities.note') }}" maxlength="191"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="saveModalCity" class="btn btn-primary" data-dismiss="modal">Lưu</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
