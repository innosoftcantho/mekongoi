<!-- districtModal -->
<div class="modal fade" id="districtModal" tabindex="-1" role="dialog" aria-labelledby="districtModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="districtModalLabel">Tạo mới quận/huyện</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="city_id">{{ __('admin/customers.city_id') }} (*)</label>
                    <select class="form-control" id="modal_city_id">
                        <option value="" disabled selected> Chọn thành phố</option>
                        @foreach ($cities as $city)
                            <option value="{{ $city->id }}" >{{ $city->city_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="district_code">{{ __('admin/districts.district_code') }} (*)</label>
                    <input type="text" class="form-control" name="district_code" id="district_code" placeholder="{{ __('admin/districts.district_code') }}" maxlength="191" value="">
                </div>
                <div class="form-group">
                    <label for="district_level">{{ __('admin/districts.district_level') }} (*)</label>
                    <input type="text" class="form-control" name="district_level" id="district_level" placeholder="{{ __('admin/districts.district_level') }}" maxlength="191" value="">
                </div>
                <div class="form-group">
                    <label for="district_name">{{ __('admin/districts.district_name') }} (*)</label>
                    <input type="text" class="form-control" name="district_name" id="district_name" required placeholder="{{ __('admin/districts.district_name') }}" maxlength="191" value="">
                </div>
                <div class="form-group">
                    <label for="note">{{ __('admin/districts.note') }}</label>
                    <textarea class="form-control" name="note" id="note" rows="3" placeholder="{{ __('admin/districts.note') }}" maxlength="191"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="saveModalDistrict" class="btn btn-primary" data-dismiss="modal">Lưu</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>