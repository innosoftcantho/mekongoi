@extends('layouts.admin')

@section('title', 'INNOSOFT')

@section('content')
<form method="POST" action="{{ route($route . '.store') }}" role="form">
@method('POST')
@csrf
    <!-- sub-Navigation -->
    <nav class="navbar navbar-light bg-white shadow">
        <div class="navbar-brand">{{ __('admin/customers.detail') }}</div>
        <div class="mr-auto">
            <a class="btn btn-outline-dark my-0" href="{{ url()->previous() == url()->current() ? route($route . '.index') : url()->previous() }}">{{ __('admin/table.back') }}</a>
        </div>
        <button type="submit" class="btn btn-success">{{ __('admin/table.save') }}</button>
    </nav>
    <!-- sub-Navigation -->
    <!-- Content -->
    <div class="bg-white div-content my-3 p-3 shadow">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="customer_code">{{ __('admin/customers.customer_code') }}</label>
                    <input type="text" class="form-control" name="customer_code" id="customer_code" required placeholder="{{ __('admin/customers.customer_code') }}" maxlength="191" value="{{ old('customer_code', $data->customer_code) }}">
                </div>
                <div class="form-group">
                    <label for="customer_name">{{ __('admin/customers.customer_name') }} (*) <small>- khách hàng là cá nhân</small></label>
                    <input type="text" class="form-control" name="customer_name" id="customer_name" required placeholder="{{ __('admin/customers.customer_name') }}" maxlength="191" value="{{ old('customer_name', $data->customer_name) }}">
                </div>
                <div class="form-group">
                    <label for="company">{{ __('admin/customers.company') }} <small>- khách hàng là tổ chức</small></label>
                    <input type="text" class="form-control" name="company" id="company" placeholder="{{ __('admin/customers.company') }}" maxlength="191" value="{{ old('company', $data->company) }}">
                </div>
                <div class="form-group">
                    <label for="tax_id">{{ __('admin/customers.tax_id') }}</label>
                    <input type="text" class="form-control" name="tax_id" id="tax_id" placeholder="{{ __('admin/customers.tax_id') }}" maxlength="191" value="{{ old('tax_id', $data->tax_id) }}">
                </div>

                <div class="form-group">
                    <label for="debt">{{ __('admin/customers.debt') }}</label>
                    <input type="number" class="form-control" name="debt" id="debt" placeholder="{{ __('admin/customers.debt') }}" value="{{ old('debt', $data->debt) }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="city_id">{{ __('admin/customers.city_id') }} (*)</label>
                    <div class="input-group">
                        <select name="city_id" class="form-control" id="city_id" required>
                            <option value="" disabled selected> Chọn thành phố</option>
                            @foreach ($cities as $city)
                                <option value="{{ $city->id }}" >{{ $city->city_name }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <button type="button" class="btn btn-success" data-modal-type="city_id" data-toggle="modal" data-target="#cityModal">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="district_id">{{ __('admin/customers.district_id') }} (*)</label>
                    <div class="input-group">
                        <select name="district_id" class="form-control" id="district_id" required>
                            <option value="" disabled selected> Chọn quận</option>
                            @foreach ($districts as $district)
                                <option value="{{ $district->id }}" >{{ $district->district_name }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#districtModal">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ward_id">{{ __('admin/customers.ward_id') }}</label>
                    <div class="input-group">
                        <select name="ward_id" class="form-control" id="ward_id">
                            <option value="" disabled selected> Chọn phường</option>
                            @foreach ($wards as $ward)
                                <option value="{{ $ward->id }}" >{{ $ward->ward_name }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#wardModal">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address">{{ __('admin/customers.address') }}</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="{{ __('admin/customers.address') }}" maxlength="191" value="{{ old('address', $data->address) }}">
                </div>
                <div class="form-group">
                    <label for="phone">{{ __('admin/customers.phone') }}</label>
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="{{ __('admin/customers.phone') }}" maxlength="191" value="{{ old('phone', $data->phone) }}">
                </div>
            </div>
            <div class="col-md-12">
                <hr class="w-100">
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="identity_old">{{ __('admin/customers.identity_old') }}</label>
                    <input type="text" class="form-control" name="identity_old" id="identity_old" placeholder="{{ __('admin/customers.identity_old') }}" maxlength="191" value="{{ old('identity_old', $data->identity_old) }}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="identity">{{ __('admin/customers.identity') }}</label>
                    <input type="text" class="form-control" name="identity" id="identity" placeholder="{{ __('admin/customers.identity') }}" maxlength="191" value="{{ old('identity', $data->identity) }}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="birthday_pick">{{ __('admin/customers.birthday')  }}</label>
                    <input type="text" class="form-control" name="birthday_pick" id="birthday_pick" placeholder="{{ __('admin/customers.birthday') }}" maxlength="191" value="{{ old('birthday', $data->birthday) }}">
                    <input type="text" hidden name="birthday" id="birthday" value="{{ now() }}" >
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="mobile">{{ __('admin/customers.mobile') }}</label>
                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="{{ __('admin/customers.mobile') }}" maxlength="191" value="{{ old('mobile', $data->mobile) }}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="note">{{ __('admin/customers.note') }}</label>
                    <textarea class="form-control" name="note" id="note" placeholder="{{ __('admin/customers.note') }}" maxlength="191" rows="5">{{ old('note', $data->note) }}</textarea>
                </div>
            </div>
        </div>
    </div>
    <!-- Content -->
</form>

@include('admin.customers.city')
@include('admin.customers.district')
@include('admin.customers.ward')
@endsection

@push('js')
    <script>

        // ----DatePicker
        $(function() {
            $('#birthday_pick').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'),10),
                startDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY'
                }
            }, function(start, end, label) {
                $("#birthday").val(start.format("YYYY-MM-DD"))
            });
        });

        // ------ Ajax City-District
        $("#city_id").on("change", function (){
            var nullOption  = new Option("Chọn quận", "", true, true);
            $("#district_id").empty();
            $("#district_id").append(nullOption);
            axios.post('{{ route('wards.city_district') }}', {
                city: $(this).val(),
            })
            .then(function (response) {
                console.log(response);
                $.each(response.data, function( index, value ) {
                    var option = new Option(value.district_name, value.id, false, false);
                    $("#district_id").append(option);
                });
                $("#district_id").val("").trigger("change");
            })
            .catch(function (error) {
                console.log(error);
            });
        })
        
        // ------ Ward Modal dropdowns
        $("#ward_modal_city_id").on("change", function (){
            var nullOption  = new Option("Chọn quận", "", true, false);
            $("#ward_modal_district_id").empty();
            $("#ward_modal_district_id").append(nullOption);
            axios.post('{{ route('wards.city_district') }}', {
                city: $(this).val(),
            })
            .then(function (response) {
                console.log(response);
                $.each(response.data, function( index, value ) {
                    var option = new Option(value.district_name, value.id, false, false);
                    $("#ward_modal_district_id").append(option);
                });
            })
            .catch(function (error) {
                console.log(error);
            });
        })
        
        // ------ Ajax District-Ward
        $("#district_id").on("change", function (){
            var nullOption  = new Option("Chọn phường", "", true, false);
            $("#ward_id").empty();
            $("#ward_id").append(nullOption);
            axios.post('{{ route('wards.district_ward') }}', {
                district: $(this).val(),
            })
            .then(function (response) {
                console.log(response);
                $.each(response.data, function( index, value ) {
                    var option      = new Option(value.ward_name, value.id, false, false);
                    $("#ward_id").append(option);
                });
            })
            .catch(function (error) {
                console.log(error);
            });
        })

        // ------ Modal JS
        $("#saveModalCity").on("click", function () {
            axios.post('{{ route('cities.store') }}', {
                city_code: $("#city_code").val(),
                city_level: $("#city_level").val(),
                city_name: $("#city_name").val(),
                note: $("#note").val(),
                is_async: 1,
            })
            .then(function (response) {
                console.log(response);
                var option      = new Option(response.data.city.city_name, response.data.city.id, false, false);
                $("#city_id").append(option).trigger('change');
                toastr[response.data.status](response.data.message);
            })
            .catch(function (error) {
                console.log(error);
                toastr["error"]("Dữ liệu không hợp lệ");
            });
        })
        
        $("#saveModalDistrict").on("click", function () {
            axios.post('{{ route('districts.store') }}', {
                district_code: $("#district_code").val(),
                district_level: $("#district_level").val(),
                district_name: $("#district_name").val(),
                city_id: $("#modal_city_id").val(),
                page_city: $("#city_id").val(),
                note: $("#note").val(),
                is_async: 1,
            })
            .then(function (response) {
                console.log(response);
                var option      = new Option(response.data.district.district_name, response.data.district.id, false, false);
                $("#district_id").append(option);
                toastr[response.data.status](response.data.message);
            })
            .catch(function (error) {
                console.log(error);
                toastr["error"]("Dữ liệu không hợp lệ");
            });
        })
        
        $("#saveModalWard").on("click", function () {
            axios.post('{{ route('wards.store') }}', {
                ward_code: $("#ward_code").val(),
                ward_level: $("#ward_level").val(),
                ward_name: $("#ward_name").val(),
                city_id: $("#ward_modal_city_id").val(),
                district_id: $("#ward_modal_district_id").val(),
                page_district: $("#district_id").val(),
                note: $("#note").val(),
                is_async: 1,
            })
            .then(function (response) {
                console.log(response);
                var option      = new Option(response.data.ward.ward_name, response.data.ward.id, false, false);
                $("#ward_id").append(option);
                toastr[response.data.status](response.data.message);
            })
            .catch(function (error) {
                console.log(error);
                toastr["error"]("Dữ liệu không hợp lệ");
            });
        })

    </script>
@endpush

@push('ready')
    @if (session('success'))
    toastr["success"]("{{ __('message.' . session('success')) }}");
    @endif
    @if (count($errors) > 0)
    toastr["error"]("@foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach");
    @endif
    $("#city_id").val("{{ old('city_id', $data->cities->first()->id) }}");
    $("#district_id").val("{{ old('district_id', $data->districts->first()->id) }}");
    $("#ward_id").val("{{ old('ward_id', $data->wards->first()->id) }}");
@endpush