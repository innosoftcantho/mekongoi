<!-- wardModal -->
<div class="modal fade" id="wardModal" tabindex="-1" role="dialog" aria-labelledby="wardModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="wardModalLabel">Tạo mới phường</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="ward_modal_city_id">{{ __('admin/customers.city_id') }} (*)</label>
                    <select class="form-control" id="ward_modal_city_id">
                        <option value="" disabled selected> Chọn thành phố</option>
                        @foreach ($cities as $city)
                            <option value="{{ $city->id }}" >{{ $city->city_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group"> 
                    <label for="ward_modal_district_id">{{ __('admin/customers.district_id') }} (*)</label>
                    <select name="" class="form-control" id="ward_modal_district_id">
                        <option value="" disabled selected> Chọn quận</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="ward_code">{{ __('admin/wards.ward_code') }} (*)</label>
                    <input type="text" class="form-control" name="ward_code" id="ward_code" placeholder="{{ __('admin/wards.ward_code') }}" maxlength="191" value="">
                </div>
                <div class="form-group">
                    <label for="ward_level">{{ __('admin/wards.ward_level') }} (*)</label>
                    <input type="text" class="form-control" name="ward_level" id="ward_level" placeholder="{{ __('admin/wards.ward_level') }}" maxlength="191" value="">
                </div>
                <div class="form-group">
                    <label for="ward_name">{{ __('admin/wards.ward_name') }} (*)</label>
                    <input type="text" class="form-control" name="ward_name" id="ward_name" required placeholder="{{ __('admin/wards.ward_name') }}" maxlength="191" value="">
                </div>
                <div class="form-group">
                    <label for="note">{{ __('admin/wards.note') }}</label>
                    <textarea class="form-control" name="note" id="note" rows="3" placeholder="{{ __('admin/wards.note') }}" maxlength="191"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="saveModalWard" class="btn btn-primary" data-dismiss="modal">Lưu</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>