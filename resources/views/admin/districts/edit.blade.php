@extends('layouts.admin')

@section('title', 'INNOSOFT')

@section('content')
<form method="POST" action="{{ route($route . '.update', ['id' => $data->id]) }}" role="form" enctype="multipart/form-data">
@method('PUT')
@csrf
    <nav class="navbar navbar-light bg-white shadow">
        <div class="navbar-brand">{{ __('admin/districts.detail') }}</div>
        <div class="mr-auto">
            <a class="btn btn-outline-dark my-0" href="{{ url()->previous() == url()->current() ? route($route . '.index') : url()->previous() }}">{{ __('admin/table.back') }}</a>
        </div>
        <button type="submit" class="btn btn-success">{{ __('admin/table.save') }}</button>
    </nav>
    <div class="bg-white div-content my-3 p-3 shadow">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="city_id">{{ __('admin/districts.city_id') }} (*)</label>
                    <select name="city_id" class="form-control" id="city_id">
                        <option value="" disabled selected> Chọn thành phố</option>
                        @foreach ($cities as $city)
                            <option value="{{ $city->id }}" >{{ $city->city_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="district_code">{{ __('admin/districts.district_code') }} (*)</label>
                    <input type="text" class="form-control" name="district_code" id="district_code" placeholder="{{ __('admin/districts.district_code') }}" maxlength="191" value="{{ old('district_code', $data->district_code) }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="district_level">{{ __('admin/districts.district_level') }} (*)</label>
                    <input type="text" class="form-control" name="district_level" id="district_level" placeholder="{{ __('admin/districts.district_level') }}" maxlength="191" value="{{ old('district_level', $data->district_level) }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="district_name">{{ __('admin/districts.district_name') }} (*)</label>
                    <input type="text" class="form-control" name="district_name" id="district_name" required placeholder="{{ __('admin/districts.district_name') }}" maxlength="191" value="{{ old('district_name', $data->district_name) }}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="note">{{ __('admin/districts.note') }}</label>
                    <textarea class="form-control" name="note" id="note" placeholder="{{ __('admin/districts.note') }}" maxlength="191" rows="5">{{ old('note', $data->note) }}</textarea>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@push('js')
    <script>
    </script>
@endpush

@push('ready')
    @if (count($errors) > 0)
    toastr["error"]("@foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach");
    @endif
    bsCustomFileInput.init();

    $('#city_id').select2();
    $("#city_id").val("{{ old('city_id', $data->city_id) }}").trigger("change")
@endpush