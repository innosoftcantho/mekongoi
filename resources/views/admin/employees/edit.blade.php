@extends('layouts.admin')

@section('title', 'INNOSOFT')

@section('content')
<form method="POST" action="{{ route($route . '.update', ['id' => $data->id]) }}" role="form" enctype="multipart/form-data">
@method('PUT')
@csrf
    <nav class="navbar navbar-light bg-white shadow">
        <div class="navbar-brand">{{ __('admin/employees.detail') }}</div>
        <div class="mr-auto">
            <a class="btn btn-outline-dark my-0" href="{{ url()->previous() == url()->current() ? route($route . '.index') : url()->previous() }}">{{ __('admin/table.back') }}</a>
        </div>
        <button type="submit" class="btn btn-success">{{ __('admin/table.save') }}</button>
    </nav>
    <div class="bg-white div-content my-3 p-3 shadow">
        <div class="row">
            <!--Input info-->
            <div class="col-md-6">
                <div class="form-group">
                    <label for="upload">{{ __('admin/employees.avatar') }}</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="upload" name="upload" value="{{ old('upload') }}">
                        <label class="custom-file-label" for="upload">{{ old('upload', $data->avatar) }}</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="employee_code">{{ __('admin/employees.employee_code') }} (*)</label>
                    <input type="text" class="form-control" name="employee_code" id="employee_code" required placeholder="{{ __('admin/employees.employee_code') }}" maxlength="191" value="{{ old('employee_code', $data->employee_code) }}">
                </div>
                <div class="form-group">
                    <label for="employee_name">{{ __('admin/employees.employee_name') }} (*)</label>
                    <input type="text" class="form-control" name="employee_name" id="employee_name" required placeholder="{{ __('admin/employees.employee_name') }}" maxlength="191" value="{{ old('employee_name', $data->employee_name) }}">
                </div>
                <div class="form-group">
                    <label for="identity_old">{{ __('admin/employees.identity_old') }}</label>
                    <input type="text" class="form-control" name="identity_old" id="identity_old" placeholder="{{ __('admin/employees.identity_old') }}" maxlength="191" value="{{ old('identity_old', $data->identity_old) }}">
                </div>
                <div class="form-group">
                    <label for="identity">{{ __('admin/employees.identity') }}</label>
                    <input type="text" class="form-control" name="identity" id="identity" placeholder="{{ __('admin/employees.identity') }}" maxlength="191" value="{{ old('identity', $data->identity) }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="birthday_pick">{{ __('admin/employees.birthday') }}</label>
                    <input type="text" class="form-control" name="birthday_pick" id="birthday_pick" placeholder="{{ __('admin/employees.birthday') }}" maxlength="191" value="{{ old('birthday_pick') }}">
                    <input type="text" hidden name="birthday" id="birthday" value="{{ $data->birthday }}">
                </div>
                <div class="form-group">
                    <label for="address">{{ __('admin/employees.address') }}</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="{{ __('admin/employees.address') }}" maxlength="191" value="{{ old('address', $data->address) }}">
                </div>
                <div class="form-group">
                    <label for="phone">{{ __('admin/employees.phone') }}</label>
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="{{ __('admin/employees.phone') }}" maxlength="191" value="{{ old('phone', $data->phone) }}">
                </div>
                <div class="form-group">
                    <label for="mobile">{{ __('admin/employees.mobile') }}</label>
                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="{{ __('admin/employees.mobile') }}" maxlength="191" value="{{ old('mobile', $data->mobile) }}">
                </div>
                <div class="form-group">
                    <label for="note">{{ __('admin/employees.note') }}</label>
                    <input type="text" class="form-control" name="note" id="note" placeholder="{{ __('admin/employees.note') }}" maxlength="191" value="{{ old('note', $data->note) }}">
                </div>
            </div>
            <!--End input info-->
        </div>
    </div>
</form>
@endsection

@push('js')
    <script>
        $(function() {
            $('#birthday_pick').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment("{{ $data->birthday }}"),
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'),10),
                locale: {
                    format: 'DD/MM/YYYY'
                }
            }, function(start, end, label) {
                $("#birthday").val(start.format("YYYY-MM-DD"));
            });
        });
    </script>
@endpush

@push('ready')
    @if (count($errors) > 0)
    toastr["error"]("@foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach");
    @endif
    bsCustomFileInput.init();
@endpush