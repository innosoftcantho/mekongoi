@extends('layouts.admin')

@section('title', 'INNOSOFT')

@section('content')
<form method="POST" action="{{ route($route . '.update', ['id' => $data->id]) }}" role="form">
@method('PUT')
@csrf
    <nav class="navbar navbar-light bg-white shadow">
        <div class="navbar-brand">{{ __('admin/providers.detail') }}</div>
        <div class="mr-auto">
            <a class="btn btn-outline-dark my-0" href="{{ url()->previous() == url()->current() ? route($route . '.index') : url()->previous() }}">{{ __('admin/table.back') }}</a>
        </div>
        <button type="submit" class="btn btn-success">{{ __('admin/table.save') }}</button>
    </nav>
    <div class="bg-white div-content my-3 p-3 shadow">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="provider_code">{{ __('admin/providers.provider_code') }} (*)</label>
                    <input type="text" class="form-control" name="provider_code" id="provider_code" required placeholder="{{ __('admin/providers.provider_code') }}" maxlength="191" value="{{ old('provider_code', $data->provider_code) }}">
                </div>
                <div class="form-group">
                    <label for="provider_name">{{ __('admin/providers.provider_name') }} (*)</label>
                    <input type="text" class="form-control" name="provider_name" id="provider_name" required placeholder="{{ __('admin/providers.provider_name') }}" maxlength="191" value="{{ old('provider_name', $data->provider_name) }}">
                </div>
                <div class="form-group">
                    <label for="tax_id">{{ __('admin/providers.tax_id') }}</label>
                    <input type="text" class="form-control" name="tax_id" id="tax_id" placeholder="{{ __('admin/providers.tax_id') }}" maxlength="191" value="{{ old('tax_id', $data->tax_id) }}">
                </div>
                <div class="form-group">
                    <label for="debt">{{ __('admin/providers.debt') }}</label>
                    <input type="text" class="form-control" name="debt" id="debt" placeholder="{{ __('admin/providers.debt') }}" maxlength="191" value="{{ old('debt', $data->debt) }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="address">{{ __('admin/providers.address') }}</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="{{ __('admin/providers.address') }}" maxlength="191" value="{{ old('address', $data->address) }}">
                </div>
                <div class="form-group">
                    <label for="phone">{{ __('admin/providers.phone') }}</label>
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="{{ __('admin/providers.phone') }}" maxlength="191" value="{{ old('phone', $data->phone) }}">
                </div>
                <div class="form-group">
                    <label for="mobile">{{ __('admin/providers.mobile') }}</label>
                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="{{ __('admin/providers.mobile') }}" maxlength="191" value="{{ old('mobile', $data->mobile) }}">
                </div>
                <div class="form-group">
                    <label for="note">{{ __('admin/providers.note') }}</label>
                    <input type="text" class="form-control" name="note" id="note" placeholder="{{ __('admin/providers.note') }}" maxlength="191" value="{{ old('note', $data->note) }}">
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@push('ready')
    @if (count($errors) > 0)
    toastr["error"]("@foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach");
    @endif
@endpush