@extends('layouts.admin')

@section('title', 'INNOSOFT')

@section('content')
<form method="POST" action="{{ route('receipts.store') }}" role="form">
@method('POST')
@csrf
    <nav class="navbar navbar-light bg-white shadow">
        <div class="navbar-brand">{{ __('admin/receipts.detail') }}</div>
        <div class="mr-auto">
            <a class="btn btn-outline-dark my-0" href="{{ url()->previous() == url()->current() ? route('receipts.index') : url()->previous() }}">{{ __('admin/table.back') }}</a>
        </div>
        <button type="submit" class="btn btn-success" onclick="$('#continue').val(0)">{{ __('admin/table.save') }}</button>
        <input type="text" class="form-control" id="continue" name="continue" value="{{ old('continue', 1) }}" hidden>
        <button type="submit" class="btn btn-primary ml-2" onclick="$('#continue').val(1)">{{ __('admin/table.save-continue') }}</button>
    </nav>

    <div class="bg-white my-3 p-3 shadow">
        <h3>{{ __('admin\receipts.receipt_info') }}</h3>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="symbole">{{ __('admin/receipts.symbole') }}</label>
                    <input type="text" class="form-control" name="symbole" id="symbole" placeholder="{{ __('admin/receipts.symbole') }}" maxlength="191" value="{{ old('symbole') }}">
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-group">
                    <label for="receipt_number">{{ __('admin/receipts.receipt_number') }} (*)</label>
                    <input type="text" class="form-control" name="receipt_number" id="receipt_number" placeholder="{{ __('admin/receipts.receipt_number') }}" maxlength="191" value="{{ old('receipt_number')}}" required>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="receipt_date_pick">{{ __('admin/receipts.receipt_date') }}</label>
                    <input type="text" class="form-control" id="receipt_date_pick" placeholder="{{ __('admin/receipts.receipt_date') }}">
                    <input type="text" class="form-control" id="receipt_date" name="receipt_date" value="{{ old('receipt_date', now()) }}" hidden>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white my-3 p-3 shadow">
        <h3>{{ __('admin\receipts.provider_info') }}</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="provider_id">{{ __('admin/receipts.provider_id') }} (*)</label>
                    <select name="provider_id" class="form-control" id="provider_id">
                        <option value="" disabled selected> Chọn nhà cung cấp</option>
                        @foreach ($providers as $provider)
                            <option value="{{ $provider->id }}" data-provider-phone="{{ $provider->phone }}" data-provider-address = "{{ $provider->address }}" data-provider-taxid = "{{ $provider->tax_id }}">{{ $provider->provider_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="form-group">
                    <label for="provider_name">{{ __('admin/receipts.provider_name') }} (*)</label>
                    <input type="text" class="form-control" name="provider_name" id="provider_name" placeholder="{{ __('admin/receipts.provider_name') }}" maxlength="191" value="{{ old('provider_name') }}" required>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tax_id">{{ __('admin/receipts.tax_id')  }}</label>
                    <input type="text" class="form-control" name="tax_id" id="tax_id" placeholder="{{ __('admin/receipts.tax_id') }}" maxlength="191" value="{{ old('tax_id') }}">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="address">{{ __('admin/receipts.address') }}</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="{{ __('admin/receipts.address') }}" maxlength="191" value="{{ old('address') }}">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="phone">{{ __('admin/receipts.phone') }}</label>
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="{{ __('admin/receipts.phone') }}" maxlength="191" value="{{ old('phone') }}">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="fax">{{ __('admin/receipts.fax') }}</label>
                    <input type="text" class="form-control" name="fax" id="fax" placeholder="{{ __('admin/receipts.fax') }}" maxlength="191" value="{{ old('fax') }}">
                </div>
            </div>
        
            <div class="col-md-12">
                <div class="form-group">
                    <label for="note">{{ __('admin/receipts.note') }}</label>
                    <textarea class="form-control" name="note" id="note" rows="3" placeholder="{{ __('admin/receipts.note') }}" value="{{ old('note')}}" ></textarea>
                </div>
            </div>
        </div>
    </div>
    <!-- Content -->
</form>
@endsection

@push('js')
    <script>
        $(function() {
            $("#receipt_date_pick").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format("YYYY"),10),
                locale: {
                    format: "DD/MM/YYYY"
                }
            }, function(start, end, label) {
                $('#receipt_date').val(start.format("YYYY-MM-DD"));
            });
        });
        $("#provider_id").on("change", function(){
             var selected_provider = $(this).children("option:selected");
            $("#address").val(selected_provider.attr("data-provider-address"));
            $("#tax_id").val(selected_provider.attr("data-provider-taxid"));
            $("#phone").val(selected_provider.attr("data-provider-phone"));
            $("#provider_name").val(selected_provider.text());
        })
    </script>
@endpush

@push('ready')
    @if (count($errors) > 0)
    toastr["error"]("@foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach");
    @endif
    $("#provider_id").val("{{ old('provider_id') }}");
@endpush