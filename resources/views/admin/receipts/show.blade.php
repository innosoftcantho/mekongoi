@extends('layouts.admin')

@section('title', 'INNOSOFT')

@section('content')
<nav class="navbar navbar-light bg-white shadow">
    <div class="navbar-brand">{{ 'Phiếu nhập số ' . $data->receipt_number . ' ngày ' . $data->receipt_date->format('d/m/Y') }} </div>
    <div class="mr-auto">
        <a class="btn btn-outline-dark my-0" href="{{ url()->previous() == url()->current() ? route('receipts.index') : url()->previous() }}">{{ __('admin/table.back') }}</a>
    </div>
    <button id="save-btn" type="submit" class="btn btn-success">{{ __('admin/table.save') }}</button>
</nav>

<div class="bg-white div-content my-3 p-3 shadow">
    <div class="row" id="receipt">
        <div class="col-md-12 table-responsive mb-3">
            <table class="table table-hover mb-0" id="receipt_products">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col" class="text-center">#</th>
                        <th scope="col">{{ __('admin\products.product_name') }}</th>
                        <th scope="col" class="text-center" style="width: 10%;">{{ __('admin\receipt-products.quantity') }}</th>
                        <th scope="col" class="text-right" style="width: 18%;">{{ __('admin\receipt-products.price') }}</th>
                        <th scope="col" class="text-right" style="width: 18%;">{{ __('admin\receipt-products.subtotal') }}</th>
                        <th scope="col" style="width: 3rem;"></th>
                        <th scope="col" style="width: 3rem;"></th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($receipt_products) == 0)
                    <tr id="no_data">
                        <td colspan="15"><center>{{ __('admin/table.no_data') }}</center></td>
                    </tr>
                    @endif
                    @foreach ($receipt_products as $index => $receipt_product)
                    <tr class="edit" data-product-id="{{ $receipt_product->product_id }}" id="order_{{ $receipt_product->order }}">
                        <th scope="row" class="text-center">{{ $receipt_product->order }}</th>
                        <td>
                            <span class="bg-transparent border-0 form-control">
                                {{ $receipt_product->product->product_name }}
                            </span>
                        </td>
                        <td>
                            <span class="bg-transparent border-0 form-control text-center format-quantity">
                                {{ number_format($receipt_product->quantity) }}
                            </span>
                            <input type="number" class="form-control text-center quantity" min="1" value="{{ $receipt_product->quantity }}">
                        </td>
                        <td>
                            <span class="bg-transparent border-0 form-control text-right format-price">
                                {{ number_format($receipt_product->price) }}
                            </span>
                            <input type="number" class="form-control text-right price" min="0" value="{{ $receipt_product->price }}">
                        </td>
                        <td>
                            <span class="bg-transparent border-0 form-control text-right format-subtotal">
                                {{ number_format($receipt_product->subtotal) }}
                            </span>
                            <input hidden type="number" class="form-control text-right subtotal" value="{{ $receipt_product->subtotal }}">
                        </td>
                        <td class="px-1">
                            <div class="btn btn-primary">
                                <i class="fas fa-pencil-alt"></i>
                                <i class="fas fa-save"></i>
                            </div>
                        </td>
                        <td class="px-1">
                            <div class="btn btn-danger" data-toggle="modal" data-target="#cfmDel" onclick="delete_receipt_product($(this))">
                                <i class="fa fa-trash"></i>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="btn btn-success" data-toggle="modal" data-target="#products_modal" id="new_row">
                <i class="fas fa-plus"></i> Thêm sản phẩm
            </div>
        </div>
        
        <div class="col-md-8 offset-md-4">
            <div class="form-group">
                <label for="subtotal">{{ __('admin/bills.subtotal') }}</label>
                <input type="number" class="form-control" name="subtotal" id="subtotal" placeholder="{{ __('admin/bills.subtotal') }}" value="{{ old('subtotal', $data->subtotal) }}">
            </div>
        </div>

        <div class="col-md-4 offset-md-4">
            <div class="form-group">
                <label for="vat">{{ __('admin/bills.vat') }}</label>
                <input type="number" class="form-control" name="vat" id="vat" placeholder="{{ __('admin/bills.vat') }}" value="{{ old('vat', $data->vat) }}">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="vat_total">{{ __('admin/bills.vat_total') }}</label>
                <input type="number" class="form-control" name="vat_total" id="vat_total" placeholder="{{ __('admin/bills.vat_total') }}" value="{{ old('vat_total', $data->vat_total) }}">
            </div>
        </div>

        <div class="col-md-8 offset-md-4">
            <div class="form-group">
                <label for="total" class="font-weight-bold">{{ __('admin/bills.total') }}</label>
                <input type="number" class="form-control" name="total" id="total" placeholder="{{ __('admin/bills.total') }}"value="{{ old('total', $data->total) }}">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="products_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sản phẩm</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <div class="modal-body" >
                <table class="table justify-content-center table-hover" id="products">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('admin\products.product_code') }}</th>
                            <th scope="col">{{ __('admin\products.product_name') }}</th>
                            <th scope="col">{{ __('admin\products.stock') }}</th>
                            <th scope="col">{{ __('admin\products.bill_price') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                        <tr id="product_{{ $product->id }}" data-id="{{ $product->id }}" data-price="{{ $product->bill_price }}" data-product-name="{{ $product->product_name }}" data-stock="{{ $product->stock }}">
                            <th scope="row">{{ $product->id }}</th>
                            <td>{{ $product->product_code }}</td>
                            <td>{{ $product->product_name }}</td>
                            <td>{{ $product->stock }}</td>
                            <td class="bill-price">{{ number_format($product->bill_price) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="cfmDel" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="cfmDel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h3>{{ __('admin/table.question_delete') }}</h3><br>
                <input  type="text" value="0" id="delete_order" />
                <button type="button" class="btn btn-danger" id="delete" data-dismiss="modal">{{ __('admin/table.delete') }}</button>
                <button class="btn btn-default" data-dismiss="modal">{{ __('admin/table.cancel') }}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    var stt = {{ count($receipt_products) }};

    function formatNumber(val) {
        return ("" + parseInt(val != "" ? val : 0)).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    }

    function formatPrice(val, id) {
        $("#" + id).html(("" + parseInt(val != "" ? val : 0)).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
    }

    function update_subtotal() {
        var sum = 0;
        $("#receipt_products .subtotal").each(function () {
            sum += parseInt($(this).val());
        });
        $("#subtotal").val(sum).trigger("change");
    }

    function delete_receipt_product(btn) {
        $("#delete_order").val(btn.closest("tr").index() + 1);
        // $("#delete_id").val(delete_id);
        // $("#delete_order").val(delete_order);
        // axios.delete('/user', {
        //     firstName: 'Fred',
        //     lastName: 'Flintstone'
        // })
        // .then(function (response) {
        //     update_subtotal();
        //     console.log(response);
        // })
        // .catch(function (error) {
        //     console.log(error);
        // });
    }

    $("#products_modal").on("show.bs.modal", function(e) {
        $('.store .btn-success').click();
    });

    $("#products tr").on("click", function() {
        $("#no_data").hide();
        $("#products_modal").modal("hide");
        var price = $(this).attr('data-price');
        stt++;
        $("#receipt_products").append(
            '<tr class="store" data-product-id="' + $(this).attr('data-id') + '" id="order_' + stt + '">' +
                '<th scope="row" class="text-center">' + stt + '</th>' +
                '<td><span class="bg-transparent border-0 form-control">' + $(this).attr('data-product-name') + '</span></td>' +
                '<td><span class="bg-transparent border-0 form-control text-center format-quantity">1</span><input type="number" class="form-control text-center quantity" min="1" value="1"></td>' +
                '<td><span class="bg-transparent border-0 form-control text-right format-price">' + formatNumber(price) + '</span><input type="number" class="form-control text-right price" min="0" value="' + price + '"></td>' +
                '<td><span class="bg-transparent border-0 form-control text-right format-subtotal">' + formatNumber(price) + '</span><input hidden type="number" class="form-control text-right subtotal" value="' + price + '"></td>' +
                '<td class="px-1"><div class="btn btn-success"><i class="fas fa-pencil-alt"></i><i class="fas fa-save"></i></div></td>' +
                '<td class="px-1"><div class="btn btn-danger" data-toggle="modal" data-target="#cfmDel" onclick="delete_receipt_product($(this))"><i class="fa fa-trash"></i></div></td>' +
            '</tr>'
        );
        update_subtotal();
    });

    $("#receipt_products").on("click", ".store .btn-success", function() {
        var button = $(this);
        var dataRow = $(this).closest("tr");
        axios.post("{{ route('receipt-products.store') }}", {
            receipt_id: {{ $data->id }},
            product_id: dataRow.attr("data-product-id"),
            quantity: dataRow.find(".quantity").val(),
            price: dataRow.find(".price").val(),
        })
        .then(function (response) {
            button.removeClass("btn-success").addClass("btn-primary").closest("tr").removeClass("store").addClass("edit");
            toastr[response.data.status](response.data.message);
        })
        .catch(function (error) {
            toastr["error"](error.response.data.message);
        });
    });
    
    $("#receipt_products").on("click", ".edit .btn-primary", function() {
        $(this).removeClass("btn-primary").addClass("btn-info").closest("tr").removeClass("edit").addClass("update");
    });

    $("#receipt_products").on("click", ".update .btn-info", function() {
        var button = $(this);
        var dataRow = $(this).closest("tr");
        axios.put("{{ route('receipt-products.update') }}", {
            receipt_id: {{ $data->id }},
            order: dataRow.find("th").html(),
            product_id: dataRow.attr("data-product-id"),
            quantity:  dataRow.find(".quantity").val(),
            price: dataRow.find(".price").val(),
        })
        .then(function (response) {
            button.removeClass("btn-info").addClass("btn-primary").closest("tr").removeClass("update").addClass("edit");
            toastr[response.data.status](response.data.message);
        })
        .catch(function (error) {
            toastr["error"](error.response.data.message);
        });
    });

    $("#delete").click(function() {
        var order = $("#delete_order").val();
        var dataRow = $("#order_" + order);
        axios.post("{{ route('receipt-products.destroy') }}", {
            _method: "DELETE",
            receipt_id: {{ $data->id }},
            order: order,
            product_id: dataRow.attr("data-product-id")
        })
        .then(function (response) {
            dataRow.remove();
            stt--;
            for (var i = order; i <= $("#receipt_products tbody tr").length; i++){
                $("#order_" + (parseInt(i) + 1) + " th").html(i);
                $("#order_" + (parseInt(i) + 1)).attr("id", "order_" + i);
            }
            toastr[response.data.status](response.data.message);
        })
        .catch(function (error) {
            toastr["error"](error.response.data.message);
        });
    });

    $("#receipt_products").on("change keyup", ".quantity, .price", function () {
        var dataRow = $(this).closest("tr");
        var quantity = dataRow.find(".quantity").val();
        dataRow.find(".format-quantity").html(formatNumber(quantity));
        // var stock = $("#product_" + dataRow.attr("data-product-id")).attr("data-stock");
        // $("#product_" + dataRow.attr("data-product-id")).attr("data-stock", stock - quantity);
        var price = dataRow.find(".price").val();
        dataRow.find(".format-price").html(formatNumber(price));
        var subtotal = quantity * price;
        dataRow.find(".subtotal").val(subtotal).trigger("change");
        dataRow.find(".format-subtotal").html(formatNumber(subtotal));
    });
    
    $("#receipt_products").on("change keyup", ".subtotal", function () {
        update_subtotal();
    });
    
    $("#receipt").on("change keyup", "#subtotal, #vat", function () {
        $("#vat_total").val(Number($("#vat").val()) * Number($("#subtotal").val()) / 100).trigger("change");
    });

    $("#vat_total").on("change keyup", function () {
        $("#total").val(Number($("#vat_total").val()) + Number($("#subtotal").val()));
    });
</script>
@endpush

@push('ready')
@if (session('success'))
toastr["success"]("{{ __('message.' . session('success')) }}");
@endif
@if (session('error'))
toastr["error"]("{{ __('message.' . session('error')) }}");
@endif
@endpush
