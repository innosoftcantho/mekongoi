@extends('layouts.admin')

@section('title', 'INNOSOFT')

@section('content')
<form method="POST" action="{{ route($route . '.update', ['id' => $data->id]) }}" role="form" enctype="multipart/form-data">
@method('PUT')
@csrf
    <nav class="navbar navbar-light bg-white shadow">
        <div class="navbar-brand">{{ __('admin/districts.detail') }}</div>
        <div class="mr-auto">
            <a class="btn btn-outline-dark my-0" href="{{ url()->previous() == url()->current() ? route($route . '.index') : url()->previous() }}">{{ __('admin/table.back') }}</a>
        </div>
        <button type="submit" class="btn btn-success">{{ __('admin/table.save') }}</button>
    </nav>
    <div class="bg-white div-content my-3 p-3 shadow">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="city_id">{{ __('admin/wards.city_id') }} (*)</label>
                            <select name="city_id" class="form-control" id="city_id">
                                <option value="" disabled selected> Chọn thành phố</option>
                                @foreach ($cities as $city)
                                    <option value="{{ $city->id }}" >{{ $city->city_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="district_id">{{ __('admin/wards.district_id') }} (*)</label>
                            <select name="district_id" class="form-control" id="district_id">
                                <option value="" disabled selected> Chọn Quận</option>
                                @foreach ($districts as $district)
                                    <option value="{{ $district->id }}" >{{ $district->district_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="ward_code">{{ __('admin/wards.ward_code') }} (*)</label>
                            <input type="text" class="form-control" name="ward_code" id="ward_code" placeholder="{{ __('admin/wards.ward_code') }}" maxlength="191" value="{{ old('ward_code', $data->ward_code) }}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="ward_level">{{ __('admin/wards.ward_level') }} (*)</label>
                            <input type="text" class="form-control" name="ward_level" id="ward_level" placeholder="{{ __('admin/wards.ward_level') }}" maxlength="191" value="{{ old('ward_level', $data->ward_level) }}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="ward_name">{{ __('admin/wards.ward_name') }} (*)</label>
                            <input type="text" class="form-control" name="ward_name" id="ward_name" required placeholder="{{ __('admin/wards.ward_name') }}" maxlength="191" value="{{ old('ward_name', $data->ward_name) }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="note">{{ __('admin/wards.note') }}</label>
                    <textarea class="form-control" name="note" id="note" placeholder="{{ __('admin/wards.note') }}" maxlength="191" rows="5">{{ old('note', $data->note) }}</textarea>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@push('js')
    <script>
        $("#city_id").on("change", function (){
            $("#district_id").empty();
            axios.post('{{ route($route . '.city_district') }}', {
                city: $(this).val(),
            })
            .then(function (response) {
                $.each(response.data, function( index, value ) {
                    var option = new Option(value.district_name, value.id, false, false);
                    $("#district_id").append(option).trigger('change');
                });
            })
            .catch(function (error) {
                console.log(error);
            });
        })
    </script>
@endpush

@push('ready')
    @if (count($errors) > 0)
    toastr["error"]("@foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach");
    @endif
    bsCustomFileInput.init();

    $('#district_id').select2();
    $("#city_id").val("{{ old('city_id', $data->district->city->id) }}").trigger("change");
    $("#district_id").val("{{ old('district_id', $data->district_id) }}").trigger("change");
@endpush