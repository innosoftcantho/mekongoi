<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <h5><i class="fas fa-times"></i></h5>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="carousel slide modal-flick my-3" data-type="multi" data-interval="3000" data-flickity='{ 
                        {{-- "groupCells": "100%", --}}
                        "pageDots": true,
                        "wrapAround": true,
                        "initialIndex": 0,
                        "draggable": ">1",
                        "cellAlign": "left",
                        {{-- "autoPlay": true, --}}
                        "freeScroll": false,
                        "friction": 0.8,
                        "selectedAttraction": 0.2} '>
                        @foreach ($all_products as $product)
                        <div class="col-12 col-md-6 col-xl-4 card-modal d-flex py-2 cell{{$product->id}}">
                            <div class="card shadow tour rounded-lg w-100">
                                <a href="/tour">
                                    <div class="image">
                                        @if ($product->show_type == 1)
                                            <iframe class="card-img-top" src="https://www.youtube.com/embed/{{ $product->embed }}?controls=0&amp;start=9" 
                                            frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; 
                                            picture-in-picture" allowfullscreen >
                                            </iframe>
                                        @else
                                            <img src="{{ asset($product->avatar)}}" class="card-img-top">
                                        @endif
                                    </div>
                                </a>
                                <div class="card-body pb-0">
                                    <a href="/tour">
                                        <div class="card-title text-justify">
                                            <h5 class="font-weight-bold">{{ $product->product_name }}</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="bot">
                                    <div class="row times">
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>8:00</button>
                                        </div>
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>8:30</button>
                                        </div>
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>9:00</button>
                                        </div>
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>9:30</button>
                                        </div>
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>10:00</button>
                                        </div>
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>10:30</button>
                                        </div>
                                    </div>
                                    <div class="row desc border-bottom py-2 mx-0">
                                        {!! $product->description !!}
                                        {{-- <ul class="text-secondary">
                                            <li>Lorem ipsum dolor sit amet, consetetur </li>
                                            <li>Lorem ipsum dolor sit amet, consetetur </li>
                                            <li>Lorem ipsum dolor sit amet, consetetur </li>
                                            <li>Lorem ipsum dolor sit amet, consetetur </li>
                                            <li>Lorem ipsum dolor sit amet, consetetur </li>
                                        </ul> --}}
                                    </div>
                                    <div class="d-flex justify-content-between flex-wrap mt-2">
                                        <div class="text-dark">Người lớn</div>
                                        <div>
                                            <span class="text-secondary"><del>1.000.000<span> đ</span></del></span>
                                            <span class="text-danger ml-2">{{ $product->bill_price }}<span> đ</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between mt-2">
                                        <div class="text-dark">Trẻ em (5-12 tuổi)</div>
                                        <div class="text-danger">500.000<span> đ</span></div>
                                    </div>
                                    <div class="d-flex justify-content-between mt-2">
                                        <div class="text-dark">Trẻ nhỏ (dưới 5 tuổi)</div>
                                        <div class="text-danger">Miễn phí</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        {{-- @for($i=0; $i<8; $i++)
                        <div class="col-12 col-md-6 col-xl-4 card-modal d-flex py-2 cell{{$i}}">
                            <div class="card shadow tour rounded-lg w-100">
                                <a href="/tour">
                                    <div class="image">
                                        @if ($i%2)
                                            <img src="{{ asset('img/test/'.(($i%5)+1).'.jpg')}}" class="card-img-top">
                                        @else
                                            <iframe class="card-img-top" src="https://www.youtube.com/embed/RL97w9SBeUE?controls=0&amp;start=10" 
                                            frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; 
                                            picture-in-picture" allowfullscreen>
                                            </iframe>
                                        @endif
                                    </div>
                                </a>
                                <div class="card-body pb-0">
                                    <a href="/tour">
                                        <div class="card-title text-justify">
                                            <h5 class="font-weight-bold">CAN THO FLOATING MARKET & WILDLIFE</h5>
                                        </div>
                                    </a>
                                </div>
                                <div class="bot">
                                    <div class="row times">
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>8:00</button>
                                        </div>
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>8:30</button>
                                        </div>
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>9:00</button>
                                        </div>
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>9:30</button>
                                        </div>
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>10:00</button>
                                        </div>
                                        <div class="col-3 my-2">
                                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>10:30</button>
                                        </div>
                                    </div>
                                    <div class="row desc border-bottom py-2 mx-0">
                                        <ul class="text-secondary">
                                            <li>Lorem ipsum dolor sit amet, consetetur </li>
                                            <li>Lorem ipsum dolor sit amet, consetetur </li>
                                            <li>Lorem ipsum dolor sit amet, consetetur </li>
                                            <li>Lorem ipsum dolor sit amet, consetetur </li>
                                            <li>Lorem ipsum dolor sit amet, consetetur </li>
                                        </ul>
                                    </div>
                                    <div class="d-flex justify-content-between flex-wrap mt-2">
                                        <div class="text-dark">Người lớn</div>
                                        <div>
                                            <span class="text-secondary"><del>1.000.000<span> đ</span></del></span>
                                            <span class="text-danger ml-2">1.000.000<span> đ</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between mt-2">
                                        <div class="text-dark">Trẻ em (5-12 tuổi)</div>
                                        <div class="text-danger">500.000<span> đ</span></div>
                                    </div>
                                    <div class="d-flex justify-content-between mt-2">
                                        <div class="text-dark">Trẻ nhỏ (dưới 5 tuổi)</div>
                                        <div class="text-danger">Miễn phí</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endfor --}}
                    </div>
                    <h5 class="font-weight-bold mb-4 mt-5">Chọn ngày khởi hành</h5>
                    <div class="carousel slide modal-flick my-3" data-type="multi" data-interval="3000" data-flickity='{ "groupCells": "100%",
                        "pageDots": false,
                        "wrapAround": true,
                        "initialIndex": 0,
                        "draggable": ">2",
                        {{-- "autoPlay": true, --}}
                        "freeScroll": false,
                        "friction": 0.8,
                        "selectedAttraction": 0.2} '>
                        @for($i=0; $i<12; $i++)
                        <div class="col-12 col-sm-4 col-lg-2 btn btn-info btn-carousel d-flex justify-content-between mx-2 py-3 px-4">
                            <div>
                                <h5>Thứ 7</h5>
                                <h5 class="text-left">06</h5>
                            </div>
                            <div class="font35px font-weight-bold">
                                26
                            </div>
                        </div>
                        @endfor
                    </div>
                    <h5 class="font-weight-bold my-4">Chọn giờ</h5>
                    <div class="row">
                        @for($i=0; $i<7; $i++)
                        <div class="col-3 col-sm-2 col-lg-1 my-2">
                            <button type="button" class="btn btn-info btn-block px-0 font16px">8:00</button>
                        </div>
                        <div class="col-3 col-sm-2 col-lg-1 my-2">
                            <button type="button" class="btn btn-block bg-secondary text-white px-0 font16px" disabled>8:00</button>
                        </div>
                        @endfor
                    </div>
                    <h5 class="font-weight-bold my-4">Chọn số lượng</h5>
                    <div class="bg-white rounded mb-4" id="rowCal">
                        <div class="row font16px p-3">
                            <div class="col-lg-3 text-center border-right my-3 my-lg-0" id="block">
                                <div>Người lớn</div>
                                <div class="text-danger font-weight-bold price mb-2" data-value="1000000">
                                    1.000.000
                                    <span> đ</span>
                                    <input type="number" class="subtot d-none" value="1000000">
                                </div>
                                <div class="input-group d-flex justify-content-center">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-outline-dark btn-minus rounded-left" type="button"><i class="fas fa-minus"></i></button>
                                    </div>
                                    <input type="text" class="form-control text-center border-secondary count" maxlength="3" value="1" name="qty">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-dark btn-plus rounded-right" type="button"><i class="fas fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-center border-right my-3 my-lg-0" id="block">
                                <div>Trẻ em (5 - 12 tuổi)</div>
                                <div class="text-danger font-weight-bold price mb-2" data-value="500000">
                                    500.000
                                    <span> đ</span>
                                    <input type="number" class="subtot d-none" value="500000">
                                </div>
                                <div class="input-group d-flex justify-content-center">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-outline-dark btn-minus rounded-left" type="button"><i class="fas fa-minus"></i></button>
                                    </div>
                                    <input type="text" class="form-control text-center border-secondary count" maxlength="3" value="1" name="qty">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-dark btn-plus rounded-right" type="button"><i class="fas fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-center border-right my-3 my-lg-0" id="block">
                                <div>Trẻ nhỏ (dưới 5 tuổi)</div>
                                <div class="text-danger font-weight-bold price mb-2" data-value="0">
                                    Miễn phí
                                    <span> đ</span>
                                    <input type="number" class="subtot d-none" value="0">
                                </div>
                                <div class="input-group d-flex justify-content-center">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-outline-dark btn-minus rounded-left" type="button"><i class="fas fa-minus"></i></button>
                                    </div>
                                    <input type="text" class="form-control text-center border-secondary count" maxlength="3" value="0" name="qty">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-dark btn-plus rounded-right" type="button"><i class="fas fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-center total-col my-3 my-lg-0">
                                <div>Tổng cộng: 
                                    <span class="text-danger font-weight-bold">
                                        <span id="total">3.500.000</span>
                                        <span> đ</span>
                                    </span>
                                </div>
                                <a href="/checkout" role="button" class="btn btn-info rounded-pill font16px px-5 mt-3">
                                    Thanh toán
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
    <script>
        //Show carousel
        $("#myModal").on( "shown.bs.modal", function( event ) {
            $(".modal-flick").flickity("resize");
        });

        // Format Number
        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
        }

        // Plus and Minus button
        $(".btn-minus").on("click", function(){
            var qty = parseInt($(this).parent().siblings("input").val());
            if($(this).closest("#block").find(".count").val()>0)
            $(this).closest("#block").find(".count").val(qty - 1).trigger("change");
        })

        $(".count").on("change keyup", function () {
            var block = $(this).closest("#block");
            var subTot = block.find(".price").attr("data-value") * $(this).val();
            block.find(".subtot").val(subTot).change();
        });

        $(".subtot").on("change", function () {
            var sum = 0;
            $(".subtot").each(function () {
                sum = sum + Number($(this).val());
            })
            $("#total").text(formatNumber(sum));
        })

        $(".btn-plus").on("click", function(){     
            var qty = parseInt($(this).parent().siblings("input").val());    
            $(this).closest("#block").find(".count").val(qty + 1).trigger("change");
        })

        // PRICE
        $(".price").on("change", function () {
            $(this).val
        })

        // Close Modal
        // $(".close").on("click", function () {
        //     $(".card-modal").removeClass("is-selected");
        // })

        // Active Card
        // $(".card-modal").on("click", function () {
        //     $(".is-selected").removeClass("is-selected");
        //     $(this).addClass("is-selected");
        // })
    </script>
@endpush