@extends('layouts.web')
@section('content')

<!-- Header Video -->

@include('web.home.video')

<!-- Section About -->

@include('web.home.about')

<!-- Section Book Tour -->

@include('web.home.tours')

<!-- Section Multi Image -->

@include('web.home.images')

@endsection
