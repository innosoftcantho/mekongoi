@section('footer')

<div class="footer text-white">
    <div class="container text-left">
        <div class="row pt-5 pb-3">
            <div class="col-12 col-lg-6">
                <div>
                    <div>
                        <img href="{{ url('/') }}" src="{{ asset('img/logowhite.png') }}" height="50" alt="">
                    </div>
                    <div class="text-justify mt-2">
                        Mekong ơi là công ty du lịch địa phương tốt nhất tại thành phố Cần Thơ chuyên về thủ công, 
                        nhóm thân mật và các tour du lịch có hướng dẫn riêng ở Cần Thơ, 
                        Châu Đốc ở đồng bằng sông Cửu Long, Việt Nam.
                    </div>
                </div>  
            </div>
            <div class="col-12 col-lg-6">
                <div class="row mb-2">
                    <div class="col-12 col-lg-6 mt-4 mt-lg-0">
                        <ul class="p-0">
                            <li class="text-uppercase font-weight-bold">liên hệ</li>
                            <li>
                                <a href="#" class="d-flex">
                                    <h5 class="mr-2"><i class="fas fa-map-marker-alt"></i></h5>
                                    <div class="ml-1">45 Mạc Thiên Tích, Xuân Khánh, Ninh Kiều, Cần Thơ</div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <h5 class="mr-1 d-inline-block"><i class="fas fa-phone-alt"></i></h5>
                                    0987654321 - 012344567
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <h5 class="mr-1 d-inline-block"><i class="fas fa-envelope"></i></h5>
                                    travel.contact@gmail.com
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-5">
                        <ul class="p-0">
                            <li class="text-uppercase font-weight-bold">thông tin</li>
                            <li><a href="#">Điều khoản</a></li>
                            <li><a href="#">Hướng dẫn đặt tour</a></li>
                            <li><a href="#">Quy định thanh toán</a></li>
                            <li><a href="#">Chính sách bảo mật</a></li>
                        </ul>                   
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright font-weight-bold py-3" align="center">
        Copyright © 2019 Mekong ơi. All Rights Reserved.
    </div>
</div>