<nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top py-lg-0">
    <div class="container">
        <button class="navbar-toggler mr-3 border-0 " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand py-0 " href="/home" id="logoNavScroll">
            <img src="{{ asset('img/logo.png') }}"  alt="">
        </a>
        <form class="form-inline d-flex d-lg-none my-0 ml-auto ml-md-none">
            <a href="#myModal" role="button" class="btn btn-outline-info rounded-pill text-uppercase font-weight-bold" data-toggle="modal">
                chọn tour ngay
            </a>
        </form>
        <div class="collapse navbar-collapse text-left my-2 my-lg-0" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                @foreach ($menus as $menu)
                <li class="nav-item">
                    <a class="nav-link text-uppercase font-weight-bold py-lg-4" href="{{ url($menu->alias) }}"> {{ $menu->menu_name }} <span class="sr-only">(current)</span></a>
                </li>
                @endforeach
                {{-- <li class="nav-item">
                    <a class="nav-link text-uppercase font-weight-bold py-lg-4" href="/home">Trang Chủ <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link text-uppercase font-weight-bold py-lg-4" href="/about">Giới thiệu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase font-weight-bold py-lg-4" href="/type">tour trong ngày</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link text-uppercase font-weight-bold py-lg-4" href="/type">tour nhiều ngày</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase font-weight-bold py-lg-4" href="/category">bài viết</a>
                </li>
                <li class="nav-item ml-0 ml-lg-1 mr-2">
                    <a class="nav-link text-uppercase font-weight-bold py-lg-4" href="/contact">liên hệ</a>
                </li> --}}
            </ul>
        </div>
        <form class="form-inline d-none d-lg-flex my-0 ml-auto ml-md-none">
            <a href="#myModal" role="button" class="btn btn-outline-info rounded-pill text-uppercase font-weight-bold" data-toggle="modal">
                chọn tour ngay
            </a>
        </form>
    </div>
</nav>





