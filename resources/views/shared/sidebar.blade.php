<nav class="navbar navbar-expand-lg navbar-dark p-0 shadow">
    <div class="collapse navbar-collapse align-items-start bg-danger sidebar" id="sidebar">
        <ul class="flex-column nav nav-pills nav-fill w-100">
            <a class="nav-header">TÁC VỤ CHÍNH</a>
            <a class="nav-link {{ $route=='bill' ? 'active' : '' }}" href="{{ route('bills.create') }}">
                <i class="fas fa-file-invoice-dollar"></i>
                <span class="pl-3">Lập hóa đơn</span>
            </a>
            <a class="nav-link {{ $route=='receipt' ? 'active' : '' }}" href="{{ route('receipts.create') }}">
                <i class="fas fa-file-invoice"></i>
                <span class="pl-3">Lập phiếu nhập</span>
            </a>
            <a class="nav-header">CHỨC NĂNG CHÍNH</a>
            <a class="nav-link  {{ $route=='bills' ? 'active' : '' }}" href="{{ route('bills.index') }}">
                <i class="fas fa-file-export"></i>
                <span class="pl-3">Phiếu xuất</span>
            </a>
            <a class="nav-link  {{ $route=='receipts' ? 'active' : '' }}" href="{{ route('receipts.index') }}">
                <i class="fas fa-file-import"></i>
                <span class="pl-3">Phiếu nhập</span>
            </a>
            <a class="nav-header">ĐIỀU HƯỚNG</a>
            <a class="nav-link {{ $route=='categories' ? 'active' : '' }}" href="{{ route('categories.index') }}">
                <i class="fas fa-bookmark"></i>
                <span class="pl-3">Danh mục</span>
            </a>
            <a class="nav-link {{ $route=='contents' ? 'active' : '' }}" href="{{ route('contents.index') }}">
                <i class="fas fa-book"></i>
                <span class="pl-3">Bài viết</span>
            </a>
            <a class="nav-link {{ $route=='menus' ? 'active' : '' }}" href="{{ route('menus.index') }}">
                <i class="fas fa-clipboard-list"></i>
                <span class="pl-3">Menu</span>
            </a>
            <a class="nav-link {{ $route=='languages' ? 'active' : '' }}" href="{{ route('languages.index') }}">
                <i class="fas fa-language"></i>
                <span class="pl-3">Ngôn ngữ</span>
            </a>
            <a class="nav-header">DANH MỤC</a>
            <a class="nav-link {{ $route=='products' ? 'active' : '' }}" href="{{ route('products.index') }}">
                <i class="fas fa-truck-loading"></i>
                <span class="pl-3">Sản phẩm</span>
            </a>
            <a class="nav-link {{ $route=='customers' ? 'active' : '' }}" href="{{ route('customers.index') }}">
                <i class="fas fa-user-tie"></i>
                <span class="pl-3">Khách Hàng</span>
            </a>
            <a class="nav-link {{ $route=='providers' ? 'active' : '' }}" href="{{ route('providers.index') }}">
                <i class="fas fa-truck"></i>
                <span class="pl-3">Nhà cung cấp</span>
            </a>
            <a class="nav-link {{ $route=='employees' ? 'active' : '' }}" href="{{ route('employees.index') }}">
                <i class="fas fa-people-carry"></i>
                <span class="pl-3">Nhân viên</span>
            </a>
            <a class="nav-header">CƠ BẢN</a>
            <a class="nav-link {{ $route=='types' ? 'active' : '' }}" href="{{ route('types.index') }}">
                <i class="fas fa-boxes"></i>
                <span class="pl-3">Loại</span>
            </a>
            <a class="nav-link {{ $route=='cities' ? 'active' : '' }}" href="{{ route('cities.index') }}">
                <i class="fas fa-city"></i>
                <span class="pl-3">Tỉnh/thành phố</span>
            </a>
            <a class="nav-link {{ $route=='districts' ? 'active' : '' }}" href="{{ route('districts.index') }}">
                <i class="fas fa-building"></i>
                <span class="pl-3">Quận/huyện</span>
            </a>
            <a class="nav-link {{ $route=='wards' ? 'active' : '' }}" href="{{ route('wards.index') }}">
                <i class="fas fa-university"></i>
                <span class="pl-3">Phường/xã</span>
            </a>
            @if (Auth::id() < 2)
            <a class="nav-header">NHÂN SỰ</a>
            <a class="nav-link {{ $route=='users' ? 'active' : '' }}" href="{{ route('users.index') }}">
                <i class="fas fa-users"></i>
                <span class="pl-3">Người dùng</span>
            </a>
            @endif

            <div class="d-lg-none">
                <div class="dropdown">
                    <a id="navbarProfile" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->fullname }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarProfile">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                    </div>
                </div>
            </div>
        </ul>
    </div>
</nav>