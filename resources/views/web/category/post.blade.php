<div class="post-section">
    <div class="bg-center bg-cover" style="background-image: url( {{ asset('img/vietnam.jpg') }} )">
        <div class="bg-overlay header">
            <h1 class="centered text-uppercase text-white font-weight-bold">bài viết</h1>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 mb-5 mt-4">
                @foreach ($contents as $content)
                <div class="card border shadow rounded mt-4">
                    <a href="{{ url($menu->alias, [$content->alias]) }}">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="image d-flex align-items-center justify-content-center">
                                    <img src="{{ asset($content->avatar)}}" height="271" class="card-img-top">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="card-body pl-md-0">
                                    <h5 class="card-title text-justify font-weight-bold">
                                        {{ $content->title }}
                                    </h5>
                                    <div class="card-text text-muted text-justify">
                                        <div class="font14px d-flex flex-wrap font-italic my-3">
                                            <div class="mr-5">Người đăng: {{ $content->user->username }}</div>
                                            <div>Ngày đăng: {{ date('d/m/Y', strtotime($content->created_at)) }}</div>
                                        </div>
                                        <div class="summary text-secondary mt-2">
                                            {!! $content->summary !!}
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
                {{-- @for($i=0; $i<5; $i++)
                    <div class="card border shadow rounded mt-4">
                        <a href="/content">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="image d-flex align-items-center justify-content-center">
                                        <img src="{{ asset('img/test/'.(($i%5)+1).'.jpg')}}" height="271" class="card-img-top">
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="card-body pl-md-0">
                                        <h5 class="card-title text-justify font-weight-bold">
                                            Giới thiệu thành phố Cần Thơ
                                        </h5>
                                        <div class="card-text text-muted text-justify">
                                            <div class="font14px d-flex flex-wrap font-italic my-3">
                                                <div class="mr-5">Người đăng: Admin</div>
                                                <div>Ngày đăng: 1/1/2019</div>
                                            </div>
                                            <div class="summary text-secondary mt-2">
                                                Cần Thơ là thành phố thủ đô của đồng bằng sông Cửu Long, Việt Nam. 
                                                Nó có dân số 1,6 triệu người vào năm 2016, và nằm ở bờ nam sông Hậu, một nhánh của sông Mê Kông. Năm 2011, sân bay quốc tế Cần Thơ khai trương

                                                Cần Thơ nằm cách Hồ Chí Minh 169 km (3,5 giờ lái xe), cách Châu Đốc 
                                                120km, 3 km (3 giờ lái xe) từ Rạch Giá và 200 km (4,5 giờ lái xe) từ Hà Tiến.                                                    
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endfor --}}
            </div>
        </div>
    </div>
</div>