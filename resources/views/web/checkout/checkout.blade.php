<div class="checkout">
    <div class="bg-center bg-cover header" style="background-image: url( {{ asset('img/dailytours.jpg') }} )">
        <div class="d-inline-block bg-info bottom-left text-uppercase text-white font-weight-bold px-2 py-1 px-md-3 py-md-2">
            CAN THO FLOATING MARKET & WILDLIFE
        </div>
    </div>
    <div class="container">
        <div class="my-5">
            <div class="bg-white shadow font16px p-4">
                <h4 class="text-center text-uppercase font-weight-bold">
                    thông tin đặt tour
                </h4>
                <hr>
                <div class="my-4">
                    <div class="text-uppercase font-weight-bold mb-2">Ghi chú</div>
                    <ul class="font14px">
                        <li>Không được bỏ trống trường có dấu <span class="text-danger">*</span></li>
                        <li>Giá vé trẻ em (5-12 tuổi) bằng 50% vé người lớn</li>
                        <li>Giá vé trẻ nhỏ (dưới 5 tuổi) được miễn phí</li>
                    </ul>
                </div>
                <div class="text-uppercase font-weight-bold mb-3">thông tin tour</div>
                <div class="row my-0 my-lg-3">
                    <div class="col-12 col-lg-4 mb-3">
                        <label>Tên tour<span class="text-danger"> *</span></label>
                        <select class="form-control" id="city">
                            <option>TP.Hồ Chí Minh</option>
                            <option>Hà Nội</option>
                            <option>Vũng Tàu</option>
                            <option>Đà Nẵng</option>
                        </select>
                    </div>
                    <div class="col-12 col-lg-4 mb-3">
                        <label>Ngày khởi hành<span class="text-danger"> *</span></label>
                        <select class="form-control" id="district">
                            <option>Thứ hai</option>
                            <option>Thứ ba</option>
                            <option>Thứ tư</option>
                            <option>Thứ năm</option>
                        </select>
                    </div>
                    <div class="col-12 col-lg-4 mb-3">
                        <label>Thời gian<span class="text-danger"> *</span></label>
                        <select class="form-control" id="village">
                            <option>Sáng</option>
                            <option>Trưa</option>
                            <option>Chiều</option>
                            <option>Tối</option>
                        </select>
                    </div>
                </div>
                <div class="row my-0 my-lg-3">
                    <div class="col-12 col-lg-4 mb-3">
                        <label>Người lớn<span class="text-danger"> *</span></label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="col-12 col-lg-4 mb-3">
                        <label>Trẻ em (5-12 tuổi)</label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="col-12 col-lg-4 mb-3">
                        <label>Trẻ nhỏ (dưới 5 tuổi)</label>
                        <input class="form-control" type="text">
                    </div>
                </div>
                <div class="text-uppercase font-weight-bold my-3">thông tin khách hàng</div>
                <div class="row my-0 my-lg-3">
                    <div class="col-12 col-lg-6 mb-3">
                        <label>Tên khách hàng<span class="text-danger"> *</span></label>
                        <input class="form-control" type="text">
                    </div>
                    <div class="col-12 col-lg-6 mb-3">
                        <label>Địa chỉ</label>
                        <input class="form-control" type="text">
                    </div>
                </div>
                <div class="row my-0 my-lg-3">
                    <div class="col-12 col-lg-6 mb-3">
                        <label>Giới tính<span class="text-danger"> *</span></label>
                        <div class="border sex d-flex justify-content-around">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadioInline1">Nam</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                                <label class="custom-control-label" for="customRadioInline2">Nữ</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 mb-3">
                        <label>Số điện thoại<span class="text-danger"> *</span></label>
                        <input class="form-control" type="text">
                    </div>
                </div>
                <div class="row my-0 my-lg-3">
                    <div class="col-12 col-lg-6 mb-3">
                        <label>Email<span class="text-danger"> *</span></label>
                        <input class="form-control" type="text">
                    </div>
                </div>
                <h4 class="bg-light rounded text-center font-weight-bold my-3 p-5">
                    Tổng cộng: <span class="text-danger"> 0<span> đ</span></span>
                </h4>
                <div class="row d-flex pt-3 px-3">
                    <div class="col-12 col-sm-6 d-flex align-items-center px-0">
                        <a href="#" class="mx-auto mx-sm-0">
                            <i class="fas fa-arrow-left mr-2"></i>Xem các tour khác
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 d-flex justify-content-end px-0">
                        <button class="btn btn-info btn-lg rounded-pill mx-auto mx-sm-0 mt-4 mt-sm-0">Đặt tour</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>