<div class="contact">
    <div class="bg-center bg-cover" style="background-image: url( {{ asset('img/bluesky.jpg') }} )">
        <div class="bg-overlay text-center header">
            <h1 class="centered text-uppercase text-white font-weight-bold">liên hệ</h1>
        </div>
    </div>
    <div class="container my-5 my-md-4">
        <div class="p-3 p-md-4">
            <div class="row font16px">
                <div class="col-md-6">
                    <h4 class="text-uppercase font-weight-bold">thông tin liên hệ</h4>
                    <div class="d-flex my-4">
                        <h4 class="mr-4"><i class="fas fa-map-marker-alt"></i></h4>
                        <span>45 Mạc Thiên Tích, Xuân Khánh, Ninh Kiều, Cần Thơ</span>
                    </div>
                    <div class="d-flex mb-4">
                        <h5 class="mr-4"><i class="fas fa-phone-alt"></i></h5>
                        <span>0987654321 - 012344567</span>
                    </div>
                    <div class="d-flex pb-2">
                        <h5 class="mr-4"><i class="fas fa-envelope"></i></h5>
                        <span>contact.travel@gmail.com</span>
                    </div>
                    <hr class="ml-0">
                    <div class="mb-4">
                        <img src="{{ asset('img/facebook.png') }}" alt="" height="40">
                        <img src="{{ asset('img/instagram.png') }}" alt="" height="40" class="mx-3">
                        <img src="{{ asset('img/youtube.png') }}" alt="" height="40">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-12 col-lg-6 mb-3">
                            <input class="form-control" type="text" placeholder="Họ và tên">
                        </div>
                        <div class="col-12 col-lg-6 mb-3">
                            <input class="form-control" type="text" placeholder="Email">
                        </div>
                    </div>
                    <textarea class="form-control my-md-2" id="exampleFormControlTextarea1" placeholder="Nội dung liên hệ" rows="8" style="resize: none;"></textarea>
                    <button class="btn btn-info btn-send text-uppercase rounded mt-3 font16px">Gửi</button>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-center bg-cover map" style="background-image: url( {{ asset('img/rectangle280.png')}} )">
    </div>
</div>