<div class="posts-detail">
    <div class="bg-center bg-cover" style="background-image: url( {{ asset($content->avatar) }} )">
        <div class="bg-overlay text-center header">
            <h1 class="centered text-uppercase text-white font-weight-bold">giới thiệu thành phố cần thơ</h1>
        </div>
    </div>
    <div class="container">
        <div class="row my-4">
            <div class="col-12 col-lg-8 my-3">
                <div class="bg-white shadow rounded py-1">
                    <div class="text-justify p-4">
                        <div class="font14px d-flex flex-wrap font-italic text-muted mb-4">
                            <div class="mr-5">Người đăng: Admin</div>
                            <div>Ngày đăng: 1/1/2019</div>
                        </div>
                        <div class="font16px">
                            Cần Thơ là thành phố thủ đô của đồng bằng sông Cửu Long, Việt Nam. Nó có dân số 1,6 triệu người vào năm 2016, và nằm ở bờ nam sông Hậu, một nhánh của sông Mê Kông. Năm 2011, sân bay quốc tế Cần Thơ khai trương
                            Cần Thơ nằm cách Hồ Chí Minh 169 km (3,5 giờ lái xe), cách Châu Đốc 120km, 3 km (3 giờ lái xe) từ Rạch Giá và 200 km (4,5 giờ lái xe) từ Hà Tiến.
                            Khí hậu của Cần Thơ là nhiệt đới và gió mùa với hai mùa: mưa, từ tháng 5 đến tháng 11; và khô, từ tháng 12 đến tháng 4.
                            NHỮNG VỊ TRÍ HÀNG ĐẦU ĐỂ THAM QUAN TẠI CAN THO: Chợ nổi Cái Răng; trang trại Cacao hữu cơ; làng nghề làm bánh phở; vườn trái cây nhiệt đới tươi tốt và cánh đồng lúa; Chợ nổi Phong Điền
                            NHỮNG ĐIỀU CẦN THỰC HIỆN Ở CAN CAN: Chuyến du lịch Mê Kông thực sự để khám phá nhiều ngôi làng hoang sơ; đạp xe dọc theo kênh rạch; phiêu lưu và ra khỏi đường đua bằng xe tay ga; khu bảo tồn chim; Thực phẩm đường phố Cần Thơ
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 mt-3 h-100">
                <div class="text-info text-uppercase font-weight-bold border-info font16px title pb-2">
                    bài viết liên quan
                </div>
                <div class="image my-4">
                    @for($i=0; $i<3; $i++)
                    <a href="">
                        <div class="bg-center bg-cover rounded-lg my-4" style="background-image: url( {{ asset('img/test/' .(($i%5)+1).'.jpg')}} )">
                            <div class="bg-overlay text-center header rounded-lg">
                                <div class="centered text-uppercase text-white font-weight-bold">CAN THO FLOATING MARKET & WILDLIFE</div>
                            </div>
                        </div>
                    </a>
                    @endfor
                </div>
            </div>
        </div>
    </div>
</div>