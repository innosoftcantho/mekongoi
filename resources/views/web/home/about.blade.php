<div class="section-about">
    <div class="container-fluid">
        <div class="row">
            @foreach ($contents as $content)
            <div class="col-12 col-lg-6 image px-0">
                <img src="{{ asset($content->avatar) }}" class="h-100">
                <div>
                    <div class="overlay">
                        <div id="text" class="d-flex flex-column text-justify text-white p-4">
                            <div class="overflow-hidden">
                                <div class="detail font16px">
                                    {{ $content->summary }}
                                </div>
                            </div>
                            <div class="d-flex justify-content-center align-items-end h-100">
                                <a href="{{ url($menu_category->alias, [$content->alias]) }}" class="btn btn-light rounded-0">
                                    Xem tất cả
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="about-box bg-info text-uppercase text-white text-center font-weight-semi-bold rounded-0 p-2 p-sm-3">
                            {{ $content->title }}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>