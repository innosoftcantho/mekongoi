<div class="bg-light">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-12 col-sm-6 col-lg-3 my-3" align="center">
                <div class="mt-2" align="center">
                    <div class="image-news">
                        <img src="{{ asset('img/quality.svg')}}" alt="shield_bg">
                    </div>
                    <h5 class="mt-3 text-center">
                        Dịch vụ chất lượng cao
                    </h5>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3 my-3" align="center">
                <div class="mt-2" align="center">
                    <div class="image-news">
                        <img src="{{ asset('img/discount.svg')}}" alt="support">
                    </div>
                    <h5 class="mt-3 text-center">
                        Giá cả phù hợp
                    </h5>
                </div>   
            </div>
            <div class="col-12 col-sm-6  col-lg-3 my-3" align="center">
                <div class="mt-2" align="center">
                    <div class="image-news">
                        <img src="{{ asset('img/phone-call.svg')}}" alt="bestprice_bg">
                    </div>
                    <h5 class="mt-3 text-center">
                        Hỗ trợ 24/7
                    </h5>
                </div>
            </div>
        </div>
    </div>
</div>