<div class="py-3">
    <div class="container">
        <div class="row my-4 d-flex justify-content-center">
            <h3 class="font-weight-bold text-uppercase text-dark">
                tour
            </h3>
        </div>
        <div class="row mb-3">
            @foreach ($products as $product)
            <div class="col-md-6 col-xl-4 mb-4">
                <div class="card shadow tour rounded-lg h-100 w-100">
                    <a href="/tour">
                        <div class="image">
                            @if ($product->show_type == 1)
                                <iframe class="card-img-top" src="https://www.youtube.com/embed/{{ $product->embed }}?controls=0&amp;" 
                                frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; 
                                picture-in-picture" allowfullscreen >
                                </iframe>
                            @else
                                <img src="{{ asset($product->avatar)}}" class="card-img-top">
                            @endif
                        </div>
                    </a>
                    <div class="card-body pb-0">
                        <a href="/tour">
                            <div class="card-title text-justify">
                                <h5 class="font-weight-bold">{{ $product->product_name }}</h5>
                            </div>
                        </a>
                    </div>
                    <div class="bot">
                        <div class="row times">
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>8:00</button>
                            </div>
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>8:30</button>
                            </div>
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>9:00</button>
                            </div>
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>9:30</button>
                            </div>
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>10:00</button>
                            </div>
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>10:30</button>
                            </div>
                        </div>
                        <div class="row border-bottom py-3 mx-0">
                            <div class="col-6 border-right text-center">
                                <h5 class="text-danger font-weight-bold">{{ number_format($product->bill_price) }} đ</h5>
                            </div>
                            <div class="col-6 border-left text-center">
                                {{-- <h5 class="text-secondary font-weight-bold"><del>1.000.000 đ<del></h5> --}}
                            </div>
                        </div>
                        <div class="row desc border-bottom py-2 mx-0">
                            {!! $product->description !!}
                        </div>
                        <div class="row d-flex justify-content-around mt-3">
                            <div class="col-6 d-flex justify-content-center align-items-center font16px">
                                <a href="{{ url($product->type()->menuType->menu->alias, [$product->alias]) }}" class="detail">
                                    Xem chi tiết
                                </a>
                            </div>
                            <div class="col-6">
                                <a href="#myModal" role="button" data-toggle="modal" data-selector=".cell{{ $product->id }}" class="btn btn-info btn-block rounded-pill font16px book">
                                    Đặt tour
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            {{-- @for($i=0; $i<8; $i++)
            <div class="col-md-6 col-xl-4 mb-4">
                <div class="card shadow tour rounded-lg h-100 w-100">
                    <a href="/tour">
                        <div class="image">
                            @if ($i%2)
                                <img src="{{ asset('img/test/'.(($i%5)+1).'.jpg')}}" class="card-img-top">
                            @else
                                <iframe class="card-img-top" src="https://www.youtube.com/embed/pJoRh9SriEE?controls=0&amp;start=9" 
                                frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; 
                                picture-in-picture" allowfullscreen >
                                </iframe>
                            @endif
                        </div>
                    </a>
                    <div class="card-body pb-0">
                        <a href="/tour">
                            <div class="card-title text-justify">
                                <h5 class="font-weight-bold">CAN THO FLOATING MARKET & WILDLIFE</h5>
                            </div>
                        </a>
                    </div>
                    <div class="bot">
                        <div class="row times">
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>8:00</button>
                            </div>
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>8:30</button>
                            </div>
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>9:00</button>
                            </div>
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>9:30</button>
                            </div>
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>10:00</button>
                            </div>
                            <div class="col-3 my-2">
                                <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>10:30</button>
                            </div>
                        </div>
                        <div class="row border-bottom py-3 mx-0">
                            <div class="col-6 border-right text-center">
                                <h5 class="text-danger font-weight-bold">1.000.000 đ</h5>
                            </div>
                            <div class="col-6 border-left text-center">
                                <h5 class="text-secondary font-weight-bold"><del>1.000.000 đ<del></h5>
                            </div>
                        </div>
                        <div class="row desc border-bottom py-2 mx-0">
                            <ul class="text-secondary">
                                <li>Lorem ipsum dolor sit amet, consetetur </li>
                                <li>Lorem ipsum dolor sit amet, consetetur </li>
                                <li>Lorem ipsum dolor sit amet, consetetur </li>
                                <li>Lorem ipsum dolor sit amet, consetetur </li>
                                <li>Lorem ipsum dolor sit amet, consetetur </li>
                            </ul>
                        </div>
                        <div class="row d-flex justify-content-around mt-3">
                            <div class="col-6 d-flex justify-content-center align-items-center font16px">
                                <a href="/tour" class="detail">
                                    Xem chi tiết
                                </a>
                            </div>
                            <div class="col-6">
                                <a href="#myModal" role="button" data-toggle="modal" data-selector=".cell{{$i}}" class="btn btn-info btn-block rounded-pill font16px book">
                                    Đặt tour
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endfor --}}
        </div>
        {{ $products->links('web.home.paginate') }}
        {{-- <nav aria-label="Page navigation" class="d-flex justify-content-center justify-content-lg-end my-4 ">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">
                            <i class="fas fa-chevron-left"></i>
                        </span>
                    </a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">
                            <i class="fas fa-chevron-right"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </nav> --}}
    </div>
</div>

@push('js')
    <script>
        $('.bot').on( 'click', '.book', function() {
            var $carousel = $('.carousel').flickity();
            var selector = $(this).attr('data-selector');
            $carousel.flickity( 'selectCell', selector );
        });

        // $("a.book").on("click", function () {
        //     var id = $(this).closest(".tour").attr("data-id");
        //     $("#book_" + id).addClass("is-selected");
        // })
    </script>
@endpush