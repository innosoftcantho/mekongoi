<div class="section-video"  style="background-image: url( {{ asset('img/dailytours.jpg') }} )">
    <div class="overlay"></div>
    <video class="d-none d-xl-block" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="https://canthorivertour.com/upload/admin/canthorivertour.mp4" type="video/mp4">
    </video>
    <div class="container h-100">
        <div class="d-flex text-center align-items-center h-100">
            <div class="text-white w-100">
                <div class="image h-100 mb-5">
                    <img href="{{ url('/') }}" src="{{ asset('img/slogan.png') }}">
                </div>
                <div>
                    <a href="#myModal" role="button" data-toggle="modal" class="btn btn-info btn-lg rounded-pill font-weight-semi-bold d-none d-sm-inline-block">
                        Chọn tour ngay
                    </a>
                    <a href="#myModal" role="button" data-toggle="modal" class="btn btn-info rounded-pill font-weight-semi-bold d-inline-block d-sm-none">
                        Chọn tour ngay
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>