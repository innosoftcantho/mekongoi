<div class="tour-detail">
    {{-- Checkout --}}
    <div class="bg-center bg-cover header" style="background-image: url( {{ asset('img/dailytours.jpg') }} )">
        <div class="d-inline-block bg-info bottom-left text-uppercase text-white font-weight-bold px-2 py-1 px-md-3 py-md-2">
            CAN THO FLOATING MARKET & WILDLIFE
        </div>
    </div>
    <div class="container">
        <div class="row my-4">
            <div class="col-12 col-lg-8 my-3">
                <div class="bg-white shadow rounded py-1">
                    <div class="text-justify p-4">
                        <div class="mb-4">
                            <div class="font16px">
                                <div class="text-uppercase font-weight-bold mb-2">những điểm nổi bật của tour</div>
                                <ul>
                                    <li>Lorem ipsum dolor sit amet, consetetur </li>
                                    <li>Lorem ipsum dolor sit amet, consetetur </li>
                                    <li>Lorem ipsum dolor sit amet, consetetur </li>
                                    <li>Lorem ipsum dolor sit amet, consetetur </li>
                                    <li>Lorem ipsum dolor sit amet, consetetur </li>
                                </ul>
                            </div>
                        </div>
                        <div class="font16px">
                            <div class="text-uppercase font-weight-bold mb-2">bài viết</div>
                            <div class="px-4">
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 mt-3 h-100">
                <div class="bg-white shadow rounded font16px mb-4 p-3">
                    <div class="d-flex justify-content-between flex-wrap">
                        <div class="text-dark">Người lớn</div>
                        <div>
                            <span class="text-secondary"><del>1.000.000<span> đ</span></del></span>
                            <span class="text-danger ml-2">1.000.000<span> đ</span></span>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between mt-2">
                        <div class="text-dark">Trẻ em (5-12 tuổi)</div>
                        <div class="text-danger">500.000<span> đ</span></div>
                    </div>
                    <div class="d-flex justify-content-between mt-2 pb-3">
                        <div class="text-dark">Trẻ nhỏ (dưới 5 tuổi)</div>
                        <div class="text-danger">Miễn phí</div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3 my-2">
                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>8:00</button>
                        </div>
                        <div class="col-3 my-2">
                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>8:30</button>
                        </div>
                        <div class="col-3 my-2">
                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>9:00</button>
                        </div>
                        <div class="col-3 my-2">
                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>9:30</button>
                        </div>
                        <div class="col-3 my-2">
                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>10:00</button>
                        </div>
                        <div class="col-3 my-2">
                            <button type="button" class="btn btn-outline-dark btn-block px-0 font16px" disabled>10:30</button>
                        </div>
                    </div>
                    <a href="#myModal" role="button" data-toggle="modal" class="btn btn-info btn-block rounded-pill font16px">
                        Đặt tour
                    </a>
                </div>
                <div>
                    <div class="text-info text-uppercase font-weight-bold border-info font16px title pb-2">
                        tour liên quan
                    </div>
                    <div class="image my-4">
                        @for($i=0; $i<3; $i++)
                        <a href="">
                            <div class="bg-center bg-cover rounded-lg my-4" style="background-image: url( {{ asset('img/test/' .(($i%5)+1).'.jpg')}} )">
                                <div class="bg-overlay text-center header rounded-lg">
                                    <div class="centered text-uppercase text-white font-weight-bold">CAN THO FLOATING MARKET & WILDLIFE</div>
                                </div>
                            </div>
                        </a>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>