<?php

use App\Http\Middleware\Activated;
use App\Http\Middleware\Admin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::middleware(['auth', Activated::class])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::namespace('Admin')->group(function () {
            Route::middleware(Admin::class)->group(function () {
            });
            
            Route::get('/', 'HomeController@index')->name('admin');
            Route::get('profile', 'UserController@profile')->name('profile');
    
            Route::post('bill-products', 'BillProductController@store')->name('bill-products.store');
            Route::post('receipt-products', 'ReceiptProductController@store')->name('receipt-products.store');
            Route::post('products/read', 'ProductController@products')->name('products.read');
    
            Route::put('bill-products', 'BillProductController@update')->name('bill-products.update');
            Route::put('receipt-products', 'ReceiptProductController@update')->name('receipt-products.update');
    
            Route::delete('bill-products', 'BillProductController@destroy')->name('bill-products.destroy');
            Route::delete('receipt-products', 'ReceiptProductController@destroy')->name('receipt-products.destroy');
    
            Route::resource('bills', 'BillController');
            Route::resource('customers', 'CustomerController')->except(['show']);
            Route::resource('cities', 'CityController')->except(['show']);
            Route::resource('categories', 'CategoryController')->except(['show']);
            Route::resource('contents', 'ContentController');
            Route::resource('menus', 'MenuController');
            Route::resource('languages', 'LanguageController')->except(['show']);
            Route::resource('districts', 'DistrictController')->except(['show']);
            Route::resource('employees', 'EmployeeController')->except(['show']);
            Route::resource('products', 'ProductController')->except(['show']);
            Route::resource('providers', 'ProviderController')->except(['show']);
            Route::resource('receipts', 'ReceiptController');
            // Route::resource('receipt-products', 'ReceiptProductController');
            Route::resource('types', 'TypeController')->except(['show']);
            Route::resource('users', 'UserController')->except(['show']);
            Route::resource('wards', 'WardController')->except(['show']);
            Route::post('city-district', 'WardController@city_district')->name('wards.city_district');
            Route::post('district-ward', 'WardController@district_ward')->name('wards.district_ward');
    
        });
    });
});

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('{menu}', 'HomeController@menu')->where('menu', '([A-Za-z0-9\-\/]+)');
});